import { flow } from 'lodash-es';
import { lastDayOfYear } from 'date-fns/esm//fp';

import router from '@/router';

import { unpickObjectProps } from '@/utils/helper';
import { formatDate } from '@/utils/format';
import { getLastDayOfNextMonth } from '@/utils/dateHelper';

export default class RouterUtils {
  static addQuery(key, value) {
    router.push({
      name: router.currentRoute.name,
      query: {
        ...router.currentRoute.query,
        [key]: value
      }
    });
  }

  static removeQuery(key) {
    const otherQuerys = unpickObjectProps(router.currentRoute.query, [key]);
    router.push({
      name: router.currentRoute.name,
      query: otherQuerys
    });
  }

  static toggleQuery(key, value) {
    if (value === null) {
      this.removeQuery(key);
    } else {
      this.addQuery(key, value);
    }
  }
}

function addTypeQuery(route, value) {
  const categoryQuery = route.meta.categoryQueryMap.find(
    category => category.value === value
  );
  // eslint-disable-next-line no-unused-vars
  const { type, ...otherQuerys } = route.query;

  // 清空 "分類" 時，網址刪除 `type` query
  router.push({
    name: route.name,
    query: value ? { ...otherQuerys, type: categoryQuery.query } : otherQuerys
  });
}

function addKeywordsQuery(route, value) {
  // eslint-disable-next-line no-unused-vars
  const { keywords, ...otherQuerys } = route.query;

  router.push({
    name: route.name,
    query: value ? { ...otherQuerys, keywords: value } : otherQuerys
  });
}

function getMatchQuery({ query, queryMap }) {
  return queryMap.find(item => item.query === query);
}

function getMatchTypeQuery(route) {
  return getMatchQuery({
    query: route.query.type,
    queryMap: route.meta.categoryQueryMap
  });
}

const CheckTimeQuery = {
  byYear(route, onComplete, onAbort) {
    if (route.query.time) {
      const yearQuery = getMatchQuery({
        query: route.query.time,
        queryMap: route.meta.yearQueryMap
      });

      if (yearQuery) {
        const year = yearQuery.value;
        const fromDate = yearQuery.value;
        const toDate = fromDate
          ? flow(lastDayOfYear, formatDate)(new Date(fromDate))
          : null;

        onComplete({ year, fromDate, toDate });
      } else {
        onAbort();
      }
    }
  },
  byMonthDay(route, onComplete, onAbort) {
    if (route.query.time) {
      const timeQueryPattern = /(?<year>\d{4})(?<month>\d{2})/;
      const { groups } = route.query.time.match(timeQueryPattern) ?? {};

      if (groups) {
        const fromDate = `${groups.year}-${groups.month}-01`;

        const lastDayOfNextMonth = getLastDayOfNextMonth(new Date(fromDate));
        const toDate = formatDate(lastDayOfNextMonth);

        onComplete({ fromDate, toDate });
      } else {
        onAbort();
      }
    }
  }
};

function checkTypeQuery(route, onComplete, onAbort) {
  if (route.query.type) {
    const categoryQuery = getMatchTypeQuery(route);

    if (categoryQuery) {
      const type = categoryQuery.value;
      onComplete(type);
    } else {
      onAbort();
    }
  }
}

function checkQueryOfTimeAndType(
  { route, timeQueryFormat },
  onCheckTypeQueryComplete,
  onCheckTimeQueryComplete
) {
  // 用來檢查 query，錯誤的 query 才會被 push 進來
  const errorQuerys = [];

  CheckTimeQuery[timeQueryFormat](
    route,
    function onComplete({ year, fromDate, toDate }) {
      onCheckTypeQueryComplete({ year, fromDate, toDate });
    },
    function onAbort() {
      errorQuerys.push('time');
    }
  );

  checkTypeQuery(
    route,
    function onComplete(type) {
      onCheckTimeQueryComplete(type);
    },
    function onAbort() {
      errorQuerys.push('type');
    }
  );

  return errorQuerys;
}

const checkTimeQueryByYear = CheckTimeQuery.byYear;
const checkTimeQueryByMonthDay = CheckTimeQuery.byMonthDay;

function getMatchedRouteMeta(route, metaKey) {
  if (route.meta[metaKey]) {
    return route.meta[metaKey];
  } else {
    for (const matchedRoute of route.matched) {
      if (matchedRoute.meta[metaKey]) {
        return matchedRoute.meta[metaKey];
      }
    }
    return null;
  }
}

export {
  addKeywordsQuery,
  addTypeQuery,
  getMatchQuery,
  getMatchTypeQuery,
  checkTimeQueryByYear,
  checkTimeQueryByMonthDay,
  checkTypeQuery,
  checkQueryOfTimeAndType,
  getMatchedRouteMeta
};
