import axios from 'axios';
import Cookie from 'js-cookie';

import { URLQueryStringify } from '@/utils/format';
import Logger from '@/utils/Logger';

const baseURL = process.env.VUE_APP_API_URL;
const loginAPI = process.env.VUE_APP_LOGIN_API_URL;
const username = process.env.VUE_APP_API_USERNAME;
const password = process.env.VUE_APP_API_PASSWORD;

const headers = {
  // 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  Accept: 'application/json',
  'Content-Type': 'application/json; charset=utf-8'
};

const backend = axios.create({
  baseURL,
  headers
});

backend.interceptors.request.use(
  config => {
    const token = Cookie.get('token');
    if (token) config.headers.token = token;
    return config;
  },
  error => Promise.reject(error)
);

backend.interceptors.response.use(
  response => {
    const { data } = response;
    if (data[0]?.message === 'no_data') {
      return [];
    } else {
      return data;
    }
  },
  async error => {
    const { status, data, config } = error?.response ?? {};
    if (status === 401) {
      // token 過期或錯誤時，要重新登入取得 token，並重發請求
      if (['time_out', 'token_error'].includes(data?.message)) {
        await API.refreshToken();
        return await API.request(config);
      }
    } else {
      return Promise.reject(error);
    }
  }
);

const API = {
  async login({ account, password }) {
    const options = {
      url: loginAPI,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      data: URLQueryStringify({ account, password })
    };
    const response = await axios(options);
    return response.data;
  },
  async refreshToken() {
    const newToken = await this.getRefreshToken();
    Cookie.set('token', newToken);
  },
  async getRefreshToken() {
    const fetchTokenAPI = `${baseURL}/app_client.php`;
    const options = {
      url: fetchTokenAPI,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      data: { username, password }
    };

    const { data } = await axios(options);
    return data.token;
  },
  async uploadImage(file) {
    const formData = new FormData();
    formData.append('file[]', file);

    const options = {
      url: `${baseURL}/class/upload.php`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
        token: Cookie.get('token')
      },
      data: formData
    };

    try {
      const { data } = await axios(options);
      return data.message;
    } catch (error) {
      // 用 `?.` 是避免 API 發生上傳失敗之外的錯誤，若有這些例外出錯就印出錯誤訊息
      const { message } = error?.response?.data;

      if (message) {
        return message;
      } else {
        throw error;
      }
    }
  },

  OpenStreetMap: {
    async fetchLatLngByRegion(region) {
      const options = {
        url: `https://nominatim.openstreetmap.org/search`,
        method: 'GET',
        params: {
          q: region,
          countrycodes: 'tw',
          polygon_geojson: 1,
          addressdetails: 1,
          format: 'jsonv2'
        }
      };
      const response = await axios(options);
      Logger.log(region, response.data);
      return response.data;
    },
    async fetchRegionByLatLng({ lat, lng }) {
      const options = {
        url: `https://nominatim.openstreetmap.org/reverse`,
        method: 'GET',
        params: {
          lat,
          lon: lng,
          zoom: 10,
          addressdetails: 1,
          format: 'jsonv2'
        }
      };
      const response = await axios(options);
      Logger.log({ lat, lng }, response.data);
      return response.data;
    }
  },

  async POST(path, data) {
    return backend.post(path, data);
  },
  async request(config) {
    return backend.request(config);
  }
};

export default API;
