import { flow } from 'lodash-es';
import {
  getMonth as _getMonth,
  addMonths,
  lastDayOfMonth,
  getYear as _getYear
} from 'date-fns/esm//fp';
import { compareAsc } from 'date-fns';

import { parseDate } from '@/utils/format';

function sortDate(dates) {
  return dates.sort((a, b) => {
    const dateA = parseDate(a.date);
    const dateB = parseDate(b.date);
    return compareAsc(dateA, dateB);
  });
}

/**
 * @param {Date | string} date
 */
function getMonth(date) {
  if (typeof date === 'string') {
    return flow(parseDate, _getMonth)(date) + 1;
  } else {
    // type 為 Date
    return _getMonth(date) + 1;
  }
}

/**
 * @param {Date | string} date
 */
function getYear(date) {
  if (typeof date === 'string') {
    return flow(parseDate, _getYear)(date);
  } else {
    // type 為 Date
    return _getYear(date);
  }
}

const getLastDayOfNextMonth = flow(lastDayOfMonth, addMonths(1));

export { sortDate, getMonth, getYear, getLastDayOfNextMonth };
