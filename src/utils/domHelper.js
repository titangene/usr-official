function isClickClosedArea(element, selectors) {
  const isContainUnclosedArea = selectors.some(selector => {
    const targetElement = element.closest(selector);
    return targetElement !== null;
  });
  return !isContainUnclosedArea;
}

export { isClickClosedArea };
