const isDevelopment = process.env.NODE_ENV === 'development';

export default {
  log(...args) {
    if (isDevelopment) {
      console.log(...args);
    }
  },
  error(...args) {
    if (isDevelopment) {
      console.error(...args);
    }
  },
  dir(arg) {
    if (isDevelopment) {
      console.dir(arg);
    }
  },
  table(arg) {
    if (isDevelopment) {
      console.table(arg);
    }
  }
};
