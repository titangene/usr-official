import { eachYearOfInterval } from 'date-fns';

function getIntervalYearOptions(startYear, endYear) {
  return eachYearOfInterval({
    start: new Date(startYear, 0, 1),
    end: new Date(endYear, 0, 1)
  })
    .map(date => {
      const year = date.getFullYear();
      return { text: year.toString(), value: year };
    })
    .reverse();
}

export { getIntervalYearOptions };
