import { get as _get } from 'lodash-es';

function debounce(func, wait) {
  let timeout;

  return function() {
    if (timeout) clearTimeout(timeout);

    timeout = setTimeout(func, wait);
  };
}

function objectToArray(object, { keyName, valueName }) {
  return Object.entries(object).map(([key, value]) => {
    return { [keyName]: key, [valueName]: value };
  });
}

function groupBy(array, reduceBy) {
  return array.reduce((groups, item, index) => {
    const key =
      typeof reduceBy === 'function'
        ? reduceBy(item, index)
        : _get(item, reduceBy);

    if (key == null) return groups;
    return {
      ...groups,
      [key]: [...(groups[key] || []), item]
    };
  }, {});
}

function groupByToArray(array, { keyName, valueName }, reduceBy) {
  const group = groupBy(array, reduceBy);
  return objectToArray(group, { keyName, valueName });
}

// 差集
function complement(arrayA, arrayB) {
  return arrayA.filter(item => !arrayB.includes(item));
}

/**
 * @description 只擷取物件中指定的 property
 * @example
 *   pickObjectProps({a: 1, b: 2, c: 3}, ['a', 'b'])
 *   // {a: 1, b: 2}
 *
 * @param {Object} object
 * @param {String[]} props
 */
function pickObjectProps(object, props) {
  return props.reduce((accumulator, prop) => {
    if (object[prop]) {
      return {
        ...accumulator,
        [prop]: object[prop]
      };
    } else {
      return { ...accumulator };
    }
  }, {});
}

/**
 * @description 只擷取物件中未指定的 property
 * @example
 *   unpickObjectProps({a: 1, b: 2, c: 3}, ['a', 'b'])
 *   // {c: 3}
 *
 * @param {Object} object
 * @param {String[]} props
 */
function unpickObjectProps(object, props) {
  const unpickProps = complement(Object.keys(object), props);
  return pickObjectProps(object, unpickProps);
}

function getYouTubeVideoId(videoURL) {
  const url = new URL(videoURL);
  return url.searchParams.has('v')
    ? url.searchParams.get('v')
    : url.pathname.substring(1);
}

function getYouTubeEmbedURL(videoURL) {
  const youTubeVideoId = videoURL ? getYouTubeVideoId(videoURL) : null;
  return youTubeVideoId
    ? `https://www.youtube.com/embed/${youTubeVideoId}`
    : null;
}

function getFacebookEmbedURL(videoURL) {
  return `https://www.facebook.com/plugins/video.php?href=${videoURL}`;
}

export {
  debounce,
  groupBy,
  groupByToArray,
  pickObjectProps,
  unpickObjectProps,
  getYouTubeVideoId,
  getYouTubeEmbedURL,
  getFacebookEmbedURL
};
