import { flow } from 'lodash-es';
import { format, parse, isMatch } from 'date-fns/esm//fp';

const dateFormatString = 'yyyy-MM-dd';
const dateTimeFormatString = 'yyyy-MM-dd HH:mm:ss';

const _formatDate = format(dateFormatString);
const formatDateTime = format(dateTimeFormatString);

const parseDate = parse(new Date())(dateFormatString);
const parseDateTime = parse(new Date())(dateTimeFormatString);
const parseDateTimeAndFormatDate = flow(parseDateTime, _formatDate);

function formatDate(datetime) {
  if (datetime === null) {
    return null;
  } else if (isMatch(dateFormatString)(datetime)) {
    return flow(parseDate, _formatDate)(datetime);
  } else if (isMatch(dateTimeFormatString)(datetime)) {
    return flow(parseDateTime, _formatDate)(datetime);
  } else {
    return _formatDate(datetime);
  }
}

function URLQueryStringify(params) {
  const searchParams = new URLSearchParams();
  for (const param of Object.entries(params)) {
    searchParams.append(...param);
  }
  return searchParams.toString();
}

const parser = new DOMParser();

function limitTextCountInHTML(contents, count) {
  if (contents == null) return '';
  // const rawHTML = contents.replaceAll('&nbsp;', '').replaceAll('&emsp;', '');
  const rawHTML = contents.replace(/&nbsp;/g, '').replace(/&emsp;/g, '');
  const document = parser.parseFromString(rawHTML, 'text/html');
  const rawContents = document.body.textContent.trim();
  return rawContents.substring(0, count);
}

function limitEnglishWordCount(content, count) {
  const limitContent = content.split(' ').slice(0, count);
  return limitContent.length < count
    ? limitContent.join(' ')
    : `${limitContent.join(' ')}...`;
}

function urlWordBreakFormat(content) {
  const urlPattern = /(?:http|https):\/\/(?:[\w-]+)(?:\.[\w-]+)+(?:[\w.,@?^=%&amp;:/~+#-]*[\w@?^=%&amp;/~+#-])?/;

  return content.replace(urlPattern, match => {
    return `<span style="word-break:break-all">${match}</span>`;
  });
}

function getUrlWordBreakHTML(content) {
  const contentElement = document.createElement('div');
  contentElement.innerHTML = content;
  const links = [...contentElement.querySelectorAll('a')];

  for (const link of links) {
    link.innerHTML = urlWordBreakFormat(link.innerText);
  }
  return contentElement.innerHTML;
}

export {
  _formatDate,
  formatDateTime,
  parseDate,
  parseDateTime,
  parseDateTimeAndFormatDate,
  formatDate,
  URLQueryStringify,
  limitTextCountInHTML,
  limitEnglishWordCount,
  urlWordBreakFormat,
  getUrlWordBreakHTML
};
