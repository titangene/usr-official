import Vue from 'vue';
import getAwesomeSwiper from 'vue-awesome-swiper/dist/exporter.esm';
import {
  Swiper as SwiperClass,
  Navigation,
  Pagination,
  Autoplay
} from 'swiper';

import 'swiper/swiper-bundle.css';

// 安裝 Swiper 模組
SwiperClass.use([Navigation, Pagination, Autoplay]);
// 註冊 Swiper 元件和 directive
Vue.use(getAwesomeSwiper(SwiperClass));
