import Vue from 'vue';

import GoogleAnalyticsPlugin from '@/plugins/GoogleAnalyticsPlugin';

Vue.use(GoogleAnalyticsPlugin);
