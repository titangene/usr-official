// import component from link example: 'ant-design-vue/lib/<component>'

import Vue from 'vue';

import '@/assets/less/vendors/ant-design-vue.less';

// ====================================
// # Navigation

import Menu from 'ant-design-vue/lib/menu';
Vue.use(Menu);

// ====================================
// # Data Display

import Badge from 'ant-design-vue/lib/badge';
Vue.use(Badge);

import Calendar from 'ant-design-vue/lib/calendar';
Vue.use(Calendar);

import Collapse from 'ant-design-vue/lib/collapse';
Vue.use(Collapse);

import Slider from 'ant-design-vue/lib/slider';
Vue.use(Slider);

// ====================================
// # Data Picker
import DataPicker from 'ant-design-vue/lib/date-picker';
Vue.use(DataPicker);

// ====================================
// # Other

import ConfigProvider from 'ant-design-vue/lib/config-provider';
Vue.use(ConfigProvider);

import Popover from 'ant-design-vue/lib/popover';
Vue.use(Popover);

import Tooltip from 'ant-design-vue/lib/tooltip';
Vue.use(Tooltip);

import Pagination from 'ant-design-vue/lib/pagination';
Vue.use(Pagination);
