import Vue from 'vue';

import '@/assets/scss/vendors/bootstrap-vue/bootstrap.scss';

import { BButton } from 'bootstrap-vue';
Vue.component('b-button', BButton);

import { NavbarPlugin } from 'bootstrap-vue';
Vue.use(NavbarPlugin);

import { ModalPlugin } from 'bootstrap-vue';
Vue.use(ModalPlugin);

import { EmbedPlugin } from 'bootstrap-vue';
Vue.use(EmbedPlugin);

// ====================================
// # Form

import { FormPlugin } from 'bootstrap-vue';
Vue.use(FormPlugin);

import { BFormGroup } from 'bootstrap-vue';
Vue.component('b-form-group', BFormGroup);

import { BFormInput } from 'bootstrap-vue';
Vue.component('b-form-input', BFormInput);

import { FormCheckboxPlugin } from 'bootstrap-vue';
Vue.use(FormCheckboxPlugin);

import { FormSelectPlugin } from 'bootstrap-vue';
Vue.use(FormSelectPlugin);

import { BFormTextarea } from 'bootstrap-vue';
Vue.component('b-form-textarea', BFormTextarea);

import { SkeletonPlugin } from 'bootstrap-vue';
Vue.use(SkeletonPlugin);
