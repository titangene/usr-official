function getMeasurementId(route, measurementIdProp = 'GA_MEASUREMENT_ID') {
  return (
    route?.meta?.[measurementIdProp] ??
    route.matched[0]?.meta?.[measurementIdProp]
  );
}

function hasMeasurementId(route) {
  return Boolean(getMeasurementId(route));
}

function isSameMeasurementId(to, from) {
  return getMeasurementId(to) === getMeasurementId(from);
}

function getMeasurementIdFromEnv(
  route,
  envNamePrefix = 'VUE_APP_GA_MEASUREMENT_ID'
) {
  return process.env[`${envNamePrefix}_${route.name}`];
}

function addMeasurementIdInRouteMeta(route) {
  route.meta ??= {};
  route.meta.GA_MEASUREMENT_ID = getMeasurementIdFromEnv(route);
  return route;
}

export {
  getMeasurementId,
  hasMeasurementId,
  isSameMeasurementId,
  addMeasurementIdInRouteMeta
};
