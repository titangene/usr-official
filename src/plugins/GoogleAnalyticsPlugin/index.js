import {
  getMeasurementId,
  hasMeasurementId,
  isSameMeasurementId
} from './utils';

const gtagURL = 'https://www.googletagmanager.com/gtag/js';

const usePluginOptions = initialOptions => {
  let _pluginOptions = initialOptions ?? {};
  const setPluginOptions = newOptions => (_pluginOptions = newOptions);
  return [_pluginOptions, setPluginOptions];
};

const [pluginOptions, setPluginOptions] = usePluginOptions({
  initLoadScript: false,
  measurementId: null,
  route: {
    subRoutesSameMeasurementId: true,
    metaProperty: 'GA_MEASUREMENT_ID'
  }
});

function gtag() {
  window.dataLayer.push(arguments);
}

function bootstrap({ to, from }, measurementId) {
  window.dataLayer = window.dataLayer ?? [];

  if (pluginOptions.route.subRoutesSameMeasurementId) {
    if (!hasMeasurementId(to) || isSameMeasurementId(to, from)) return;
    const _measurementId = measurementId ?? getMeasurementId(to);
    const pagePath = getSendPagePath(to);

    loadScript(_measurementId, pagePath);
  } else {
    const _measurementId = measurementId ?? pluginOptions.measurementId;
    loadScript(_measurementId);
  }
}

function loadScript(measurementId, pagePath = '/') {
  // 若已載入腳本，就無需再次載入腳本，只需變更 GA 的 propery ID
  if (document.querySelector('#GoogleAnalyticsScript')) {
    gtag('config', measurementId, { page_path: pagePath });
    return;
  }

  if (!measurementId) {
    console.error('[GA] 未提供 GA measurementId，無法載入 gtag.js');
    return;
  }

  const head = document.head || document.getElementsByTagName('head')[0];
  const script = document.createElement('script');

  script.id = 'GoogleAnalyticsScript';
  script.async = true;
  script.src = `${gtagURL}?id=${measurementId}`;

  script.onload = () => {
    gtag('js', new Date());
    gtag('config', measurementId, { page_path: pagePath });

    if (process.env.NODE_ENV === 'development') {
      console.log(`[GA] 已載入 gtag.js (id: ${measurementId})`);
    }
  };

  script.onerror = () => {
    console.error('[GA] 因一些錯誤而無法載入 gtag.js');
  };

  head.appendChild(script);
}

function install(Vue, options) {
  setPluginOptions({ ...pluginOptions, ...options });
  if (!pluginOptions.initLoadScript) return;

  bootstrap(pluginOptions.measurementId);
}

function getSendPagePath(route) {
  return route.path === '/' ? '/' : route.matched[0].path;
}

export { bootstrap, gtag };

export default install;
