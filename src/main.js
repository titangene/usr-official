import Vue from 'vue';
import router from './router';
import store from './store';
import i18n from './plugins/vue-i18n';

// Swiper Vue
import './plugins/swiper-vue.js';

// Ant Design Vue
import './plugins/ant-design-vue.js';

// Bootstrap Vue
import './plugins/bootstrap-vue.js';

// Vue Leaflet
import './plugins/vue2-leaflet.js';

// Vue Meta
import './plugins/vue-meta';

// Sass
import './assets/scss/main.scss';

import App from './App.vue';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app');
