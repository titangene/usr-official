export default class OrganizationMemberEntry {
  constructor(data = {}) {
    this.id = data.id;
    this.name = data.name;
    this.picture = data.picture;
    this.project_job = data.project_job;
    this.group_name = data.group_name;
    this.district_name = data.district_name;
    this.job_title = data.job_title;
    this.phone = data.phone;
    this.introduction = data.introduction;
  }
}
