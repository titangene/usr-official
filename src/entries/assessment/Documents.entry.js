export default class DocumentsEntry {
  constructor(data = {}) {
    this.id = data.id;
    this.date = data.date;
    this.title = data.title;
    this.image = data.image;
    this.description = data.description;
    this.content = data.content;
    this.has_attachment = data.has_attachment;
  }
}
