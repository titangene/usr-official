export default class WorkshopsEntry {
  constructor(data = {}) {
    this.id = data.id;
    this.year = data.year;
    this.title = data.title;
    this.content = data.content;
    this.youtube = data.youtube;
    this.fb_video = data.fb_video;
    this.image = data.image;
    this.link = data.link;
    this.attachment = data.attachment;
    this.has_attachment = data.has_attachment;
  }
}
