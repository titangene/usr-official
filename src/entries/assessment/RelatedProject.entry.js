import { parseISO, isFuture, isToday } from 'date-fns';

export default class RelatedProjectEntry {
  constructor(data = {}) {
    this.recordId = data.recordId;
    this.id = data.id;
    this.org = data.org;
    this.title = data.title;
    this.intro = data.intro;
    this.ta = data.ta;
    this.schedule = data.schedule;
    this.link = data.link;
    this.image = data.image;
    this.due_date = data.due_date;
  }

  get isCallingForPapers() {
    const dueDate = parseISO(this.data.due_date);
    return isToday(dueDate) || isFuture(dueDate);
  }
}
