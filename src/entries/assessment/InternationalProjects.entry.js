export default class InternationalProjectsEntry {
  constructor(data = {}) {
    this.id = data.id;
    this.title = data.title;
    this.content = data.content;
    this.logo = data.logo;
    this.link = data.link;
  }
}
