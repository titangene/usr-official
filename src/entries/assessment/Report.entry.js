import { chunk } from 'lodash-es';

export default class ReportEntry {
  constructor(data = {}) {
    this.id = data.id;
    this.school = data.school;
    this.logo = data.logo;
    this.type = data.type;
    this.year = data.year;

    this.attachment = this.parseAttachmentRaw(data.attachment);
    this.hasAttachment = data.has_attachment;
  }

  parseAttachmentRaw(attachmentRaw) {
    if (attachmentRaw == null) return null;

    const attachmentEntries = attachmentRaw.split('\n');
    if (attachmentEntries.length > 0) {
      const [name, url] = chunk(attachmentEntries, 2)[0];
      return { name, url };
    } else {
      return null;
    }
  }
}
