export default class ToolBoxEntry {
  constructor(data = {}) {
    this.id = data.id;
    this.date = data.date;
    this.title = data.title;
    this.speaker = data.speaker;
    this.content = data.content;
    this.youtube = data.youtube;
    this.fb_video = data.fb_video;
    this.image = data.image;
    this.link = data.link;
    this.attachment = data.attachment;
    this.has_attachment = data.has_attachment;
  }
}
