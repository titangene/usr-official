export default class ExhibitionTopPostEntry {
  constructor(data = {}) {
    this.id = data.id;
    this.title = data.title;
    this.year = data.year;
    this.image = data.image;
    this.youtube1 = data.youtube1;
    this.youtube2 = data.youtube2;
    this.content = data.content;
    this.manual = data.manual;
    this.result = data.result;
  }
}
