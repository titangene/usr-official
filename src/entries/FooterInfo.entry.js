export default class FooterInfoEntry {
  constructor(data = {}) {
    this.id = data.id;
    this.address = data.address;
    this.address_google_maps_link = data.address_google_maps_link;
    this.tel = data.tel;
    this.email = data.email;
  }
}
