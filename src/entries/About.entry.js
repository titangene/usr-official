export default class AboutEntry {
  constructor(data = {}) {
    this.id = data.id;
    this.title = data.title;
    this.text = data.text;
    this.image = data.image;
  }
}
