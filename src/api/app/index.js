import _AboutAPI from './About.api';
import _AboutENAPI from './AboutEN.api';
import _FooterInfoAPI from './Footer.api';
import _FooterInfoENAPI from './FooterEN.api';
import _OrganizationAPI from './Organization.api';
import _OrganizationENAPI from './OrganizationEN.api';
import _ExhibitionTopPostsAPI from './ExhibitionTopPosts.api';
import _ExpoLinksAPI from './ExpoLinks.api';
import DocumentsAPI from './assessment/Documents.api';
import ReportsAPI from './assessment/Reports.api';
import WorkshopAPI from './assessment/Workshops.api';
import ToolBoxAPI from './assessment/ToolBox.api';
import RelatedProjectsAPI from './assessment/RelatedProjects.api';
import ReportTypeOptionsAPI from './assessment/ReportTypeOptions.api';
import InternationalProjectsAPI from './assessment/InternationalProjects.api';
import DynamicOptionsAPI from './DynamicOptions.api';

/**
 * @description Footer
 */
export const FooterAPI = Object.freeze({
  zh: new _FooterInfoAPI('appCPwD2XpLR68RNW/Table 0.2-Footer'),
  en: new _FooterInfoENAPI('appCPwD2XpLR68RNW/Table 0.2en-Footer')
});

/**
 * @description About
 */
export const AboutAPI = Object.freeze({
  zh: new _AboutAPI('appCPwD2XpLR68RNW/Table 0.3-About'),
  en: new _AboutENAPI('appCPwD2XpLR68RNW/Table 0.3en-About')
});

/**
 * @description 組織架構
 */
export const OrganizationAPI = Object.freeze({
  zh: new _OrganizationAPI('appCPwD2XpLR68RNW/Table 0.4-Team'),
  en: new _OrganizationENAPI('appCPwD2XpLR68RNW/Table 0.4en-Team')
});

export const ExhibitionTopPostsAPI = new _ExhibitionTopPostsAPI(
  'appCPwD2XpLR68RNW/Table 0.5-線上常設展'
);

export const ExpoLinksAPI = new _ExpoLinksAPI(
  'appCPwD2XpLR68RNW/Table 0.1-Expo'
);

export const ExhibitionYearOptionsAPI = new DynamicOptionsAPI(
  'year',
  'appuhpj9ZYdVNcmwl/Table 1-線上常設展'
);

export const BlogYearOptionsAPI = new DynamicOptionsAPI(
  'year',
  'appuhpj9ZYdVNcmwl/Table 2-專欄文章'
);

export const MediaYearOptionsAPI = new DynamicOptionsAPI(
  'year',
  'appuhpj9ZYdVNcmwl/Table 3-媒體報導'
);

export const ActivityYearOptionsAPI = new DynamicOptionsAPI(
  'year',
  'appuhpj9ZYdVNcmwl/Table 4-中心活動'
);

export const AnnouncementYearOptionsAPI = new DynamicOptionsAPI(
  'year',
  'appuhpj9ZYdVNcmwl/Table 4-計畫公告'
);

export const AssessmentAPI = Object.freeze({
  /**
   * @description 作業須知
   */
  documents: new DocumentsAPI('appJ15pZom8Tpoglo/Table 1-作業須知'),

  /**
   * @description 報告書
   */
  reports: new ReportsAPI('appwFAM9JuOq2yATx/Table 2-報告書'),

  /**
   * @description 報告書「選擇分類」選項
   */
  reportTypeOptions: new ReportTypeOptionsAPI('appwFAM9JuOq2yATx/Table 2-type'),

  /**
   * @description 培力活動
   */
  workshops: new WorkshopAPI('appkG2dOD4dkYwqaQ/Table 3-培力活動'),

  /**
   * @description 效益評估工具
   */
  toolboxes: new ToolBoxAPI('app02AyGUOaP7RWMk/Table 4-效益評估工具'),

  /**
   * @description 部會相關資源
   */
  relatedProjects: new RelatedProjectsAPI(
    'appi4iC4I5XV9gvEj/Table 5-部會相關資源'
  ),

  /**
   * @description 國際USR資源
   */
  internationalProjects: new InternationalProjectsAPI(
    'appHrucCaX0rltHxt/Table 6-國際USR資源'
  )
});
