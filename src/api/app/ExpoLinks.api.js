import { AirtableAPI } from '@/api/airtable';

export default class ExpoLinksAPI extends AirtableAPI {
  constructor(apiPath) {
    super(apiPath);
  }

  async list() {
    const records = await super.all();
    return records.map(record => this.createEntry(record));
  }

  createEntry(record) {
    const data = record.fields;
    return {
      year: data.year,
      url: data.expo_url
    };
  }
}
