import { reduce } from 'lodash-es';

import { AirtableAPI } from '@/api/airtable';
import { FooterInfoEntry } from '@/entries';

export default class FooterAPI extends AirtableAPI {
  constructor(apiPath) {
    super(apiPath);
  }

  async list() {
    const record = await super.first();
    return this.createEntry(record);
  }

  createEntry(record) {
    const initialValue = {
      address: null,
      address_google_maps_link: null,
      tel: null,
      email: null
    };

    const transformData = (result, value, key) => ({
      ...result,
      [key]: value?.trim?.() ?? value
    });

    /* eslint-disable */
    const data = reduce(
      { ...initialValue, ...record.fields },
      transformData,
      { recordId: record.id }
    );
    /* eslint-enable */
    return new FooterInfoEntry(data);
  }
}
