import { reduce } from 'lodash-es';

import { AirtableAPI } from '@/api/airtable';
import { RelatedProjectEntry } from '@/entries';

export default class RelatedProjectsAPI extends AirtableAPI {
  constructor(apiPath) {
    super(apiPath);
  }

  async get(id) {
    const record = await super.get(id);
    return this.createEntry(record);
  }

  async *eachPage(options = {}) {
    for await (const records of super.eachPage(options)) {
      yield records.map(record => this.createEntry(record));
    }
  }

  createEntry(record) {
    const initialValue = {
      org: null,
      title: null,
      intro: null,
      ta: null,
      schedule: null,
      link: null,
      image: null,
      due_date: null
    };

    const transformData = (result, value, key) => ({
      ...result,
      [key]: value?.trim?.() ?? value
    });

    /* eslint-disable */
    const data = reduce(
      { ...initialValue, ...record.fields },
      transformData,
      { recordId: record.id }
    );
    /* eslint-enable */
    return new RelatedProjectEntry(data);
  }
}
