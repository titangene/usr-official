import { reduce } from 'lodash-es';

import { AirtableAPI } from '@/api/airtable';
import { InternationalProjectsEntry } from '@/entries';

export default class ReportsAPI extends AirtableAPI {
  constructor(apiPath) {
    super(apiPath);
  }

  async get(id) {
    const record = await super.get(id);
    return this.createEntry(record);
  }

  async *eachPage(options = {}) {
    for await (const records of super.eachPage(options)) {
      yield records.map(record => this.createEntry(record));
    }
  }

  createEntry(record) {
    const initialValue = {
      title: null,
      content: null,
      logo: null,
      link: null
    };

    const transformData = (result, value, key) => ({
      ...result,
      [key]: value?.trim?.() ?? value
    });

    /* eslint-disable */
    const data = reduce(
      { ...initialValue, ...record.fields },
      transformData,
      { recordId: record.id }
    );
    /* eslint-enable */
    return new InternationalProjectsEntry(data);
  }
}
