import { AirtableAPI } from '@/api/airtable';

export default class ReportTypeOptionsAPI extends AirtableAPI {
  constructor(apiPath) {
    super(apiPath);
  }

  async list() {
    const records = await super.all();
    return records.map(record => record.fields.type);
  }
}
