import { reduce } from 'lodash-es';

import { AirtableAPI } from '@/api/airtable';
import { OrganizationMemberEntry } from '@/entries';

export default class OrganizationAPI extends AirtableAPI {
  constructor(apiPath) {
    super(apiPath);
  }

  async list() {
    const records = await super.all();
    return records.map(record => this.createEntry(record));
  }

  createEntry(record) {
    const initialValue = {
      name: null,
      picture: null,
      project_job: null,
      group_name: null,
      district_name: null,
      job_title: null,
      phone: null,
      introduction: null
    };

    const transformData = (result, value, key) => ({
      ...result,
      [key]: value?.trim?.() ?? value
    });

    /* eslint-disable */
    const data = reduce(
      { ...initialValue, ...record.fields },
      transformData,
      { recordId: record.id }
    );
    /* eslint-enable */
    return new OrganizationMemberEntry(data);
  }
}
