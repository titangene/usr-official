import { reduce } from 'lodash-es';

import { AirtableAPI } from '@/api/airtable';
import { AboutEntry } from '@/entries';

export default class FooterAPI extends AirtableAPI {
  constructor(apiPath) {
    super(apiPath);
  }

  async list() {
    const record = await super.first();
    return this.createEntry(record);
  }

  createEntry(record) {
    const initialValue = {
      title: null,
      text: null,
      image: null
    };

    const transformData = (result, value, key) => ({
      ...result,
      [key]: value?.trim?.() ?? value
    });

    /* eslint-disable */
    const data = reduce(
      { ...initialValue, ...record.fields },
      transformData,
      { recordId: record.id }
    );
    /* eslint-enable */
    return new AboutEntry(data);
  }
}
