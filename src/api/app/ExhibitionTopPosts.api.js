import { reduce } from 'lodash-es';

import { AirtableAPI } from '@/api/airtable';
import { ExhibitionTopPostEntry } from '@/entries';

export default class ExhibitionTopPostsAPI extends AirtableAPI {
  constructor(apiPath) {
    super(apiPath);
  }

  async first() {
    const record = await super.first();
    return this.createEntry(record);
  }

  async getPostByYear(year) {
    const record = await super.first({
      filters: { year: { equals: Number(year) } }
    });
    return this.createEntry(record);
  }

  createEntry(record) {
    const initialValue = {
      year: null,
      image: null,
      youtube1: null,
      youtube2: null,
      content: null,
      manual: null,
      result: null
    };

    const transformData = (result, value, key) => ({
      ...result,
      [key]: value?.trim?.() ?? value
    });

    /* eslint-disable */
    const data = reduce(
      { ...initialValue, ...record.fields },
      transformData,
      { recordId: record.id }
    );
    /* eslint-enable */
    return new ExhibitionTopPostEntry(data);
  }
}
