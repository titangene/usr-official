import { AirtableAPI } from '@/api/airtable';

export default class DynamicOptionsAPI extends AirtableAPI {
  constructor(key, apiPath) {
    super(apiPath);
    this.key = key;
  }

  async list() {
    const records = await super.all();
    return records.map(record => record.fields[this.key]);
  }
}
