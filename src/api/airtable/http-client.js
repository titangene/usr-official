import axios from 'axios';

const AIRTABLE_API_KEY = process.env.VUE_APP_AIRTABLE_API_KEY;

const airtableClient = axios.create({
  baseURL: 'https://api.airtable.com/v0',
  headers: {
    Authorization: `Bearer ${AIRTABLE_API_KEY}`,
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
});

// airtableClient.interceptors.request.use(
//   config => config,
//   error => Promise.reject(error)
// );

airtableClient.interceptors.response.use(
  response => response.data,
  error => Promise.reject(error)
);

export default airtableClient;
