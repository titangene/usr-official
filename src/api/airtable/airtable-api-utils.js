export default class AirtableAPIUtils {
  /**
   * @param {Array} options 過濾條件
   * @returns {string|null} 用於 `filterByFormula` query 的公式
   * @example
   * getFilterByFormula({
   *   AND: [
   *     { title: { search: 'xxx' }},
   *     { year: { equals: 2021 }}
   *   ]
   * })
   * // { filterByFormula: 'AND(SEARCH(LOWER("xxx"), LOWER({title})), year = 2021' }
   * getFilterByFormula({
   *   AND: [{ date: { year: 2021 }}]
   * })
   * // { filterByFormula: 'YEAR({date}) = 2021)' }
   */
  static getFilterByFormulaQuery(options = {}) {
    if (options == null || Object.keys(options).length === 0) {
      return {};
    }
    if (options.AND?.length > 0) {
      const andFormula = this.getAndFormula(options.AND);
      return { filterByFormula: andFormula };
    }

    if (Object.keys(options).length > 0) {
      const formula = this.getFormula(options);
      return { filterByFormula: formula };
    } else {
      return {};
    }
  }

  static getFormula(condition) {
    const [field, expression] = Object.entries(condition)[0];
    const [operator, operand] = Object.entries(expression)[0];

    switch (operator) {
      case 'search': {
        const getSearchFormula = keyword =>
          `SEARCH(LOWER("${keyword}"), LOWER({${field}}))`;

        if (typeof operand === 'string') {
          const keyword = operand.trim();
          return keyword.length > 0 ? getSearchFormula(keyword) : null;
        } else {
          return getSearchFormula(operand);
        }
      }
      case 'equals': {
        if (typeof operand === 'string') {
          const keyword = operand.trim();
          return keyword.length > 0 ? `${field} = "${keyword}"` : null;
        } else {
          return `${field} = ${operand}`;
        }
      }
      case 'year':
        return `YEAR({${field}}) = ${operand}`;
      default:
        return null;
    }
  }

  static getAndFormula(conditions = []) {
    const andConditions = conditions
      .map(this.getFormula)
      .filter(condition => condition !== null);

    switch (true) {
      case andConditions.length === 1: {
        return andConditions[0];
      }
      case andConditions.length >= 2: {
        const expressionArguments = andConditions.join(', ');
        return `AND(${expressionArguments})`;
      }
      default:
        return null;
    }
  }
}
