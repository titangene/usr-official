import AirtableQuery from './airtable-query';

class RecordNotFoundError extends Error {
  constructor(message) {
    super(message);
    this.name = 'RecordNotFoundError';
  }
}

export default class AirtableAPI {
  #apiPath;

  constructor(apiPath) {
    this.#apiPath = apiPath;
  }

  async first(options = {}) {
    const { records } = await AirtableQuery.getRecords(this.#apiPath, {
      view: 'Grid view',
      maxRecords: 1,
      ...options
    });
    if (records.length === 1) {
      return records[0];
    } else {
      throw new RecordNotFoundError(
        `"${this.#apiPath}" table has no match record`
      );
    }
  }

  async get(id) {
    const { records } = await AirtableQuery.getRecords(this.#apiPath, {
      filterByFormula: `{id} = ${id}`
    });
    if (records.length > 0) {
      return records[0];
    } else {
      throw new RecordNotFoundError(
        `"${this.#apiPath}" table has no record with id ${id}`
      );
    }
  }

  async all(options = {}) {
    return await AirtableQuery.getAllRecords(this.#apiPath, {
      view: 'Grid view',
      ...options
    });
  }

  eachPage(options = {}) {
    return AirtableQuery.getEachPageRecords(this.#apiPath, {
      view: 'Grid view',
      ...options
    });
  }

  // TODO: 之後直接回傳 `else` 內的東西
  async list(...args) {
    if (typeof args[0] === 'function') {
      const [callback, options = {}] = args;
      return await this._listWithPaging(callback, options);
    } else {
      const [options = {}] = args;
      return await this._list(options);
    }
  }

  async _list(options = {}) {
    return await AirtableQuery.getAllRecords(this.#apiPath, {
      view: 'Grid view',
      ...options
    });
  }

  async _listWithPaging(callback, options = {}) {
    const allRecords = AirtableQuery.getAllPagesRecords(this.#apiPath, {
      view: 'Grid view',
      ...options
    });

    let result = [];
    let index = 0;
    for await (const records of allRecords) {
      callback(records, index);
      result.push(...records);
      index++;
    }
    return result;
  }
}
