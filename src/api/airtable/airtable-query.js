import airtableClient from './http-client';
import AirtableAPIUtils from './airtable-api-utils';
import { unpickObjectProps as unpick } from '@/utils/helper';

export default class AirtableQuery {
  static async getRecordById(path, recordId, query = {}) {
    return await airtableClient.get(`/${path}/${recordId}`, {
      params: query
    });
  }

  static async getRecords(path, options = {}) {
    const { filters = {}, ...query } = options;
    return await airtableClient.get(`/${path}`, {
      params: {
        ...AirtableAPIUtils.getFilterByFormulaQuery(filters),
        ...query
      }
    });
  }

  static async getAllRecords(path, options = {}) {
    let allRecords = [];
    for await (const records of this.getEachPageRecords(path, options)) {
      allRecords.push(...records);
    }
    return allRecords;
  }

  static async *getAllPagesRecords(path, options = {}) {
    const { records, offset } = await this._getFirstPageRecords(path, options);
    yield records;

    if (offset === undefined) return;

    // 取得剩餘頁面的 records
    const restPageRecords = this.getEachPageRecords(path, {
      ...unpick(options, ['pageSize']),
      offset
    });
    for await (const records of restPageRecords) {
      yield records;
    }
  }

  static async _getFirstPageRecords(path, options = {}) {
    const { firstPageSize = 10, ...restOptions } = options;
    return await this.getRecords(path, {
      pageSize: firstPageSize,
      ...restOptions
    });
  }

  static async *getEachPageRecords(path, options = {}) {
    let hasNextPage = false;
    let nextOffset = null;
    do {
      const { offset, records } = await this.getRecords(path, {
        ...options,
        ...(nextOffset ? { offset: nextOffset } : {})
      });
      yield records;

      if (offset) {
        hasNextPage = true;
        nextOffset = offset;
      } else {
        hasNextPage = false;
        nextOffset = null;
      }
    } while (hasNextPage);
  }
}
