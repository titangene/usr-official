import Vue from 'vue';
import VueRouter from 'vue-router';
import Cookie from 'js-cookie';

import API from '@/utils/BackendAPI';

import { bootstrap as bootstrapGoogleAnalytics } from '@/plugins/GoogleAnalyticsPlugin';
import * as GA from '@/plugins/GoogleAnalyticsPlugin/utils';

import indexRoutes from './index.route';
import aboutRoutes from './about.route';
import newsRoutes from './news.route';
import resultRoutes from './result.route';
import projectRoutes from './project.route';
import mediaRoutes from './media.route';
import siteRoutes from './site.route';
import assessmentRoutes from './assessment.route';

// const Home = () => import(/* webpackChunkName: "home" */ '@/views/Home.vue');

Vue.use(VueRouter);

const routes = [
  {
    path: '*',
    redirect: { name: 'Home' }
  },
  ...indexRoutes,
  ...aboutRoutes,
  ...newsRoutes,
  ...resultRoutes,
  ...projectRoutes,
  ...mediaRoutes,
  ...siteRoutes,
  ...assessmentRoutes
].map(GA.addMeasurementIdInRouteMeta);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, _from, _savedPosition) {
    const initPosition = { x: 0, y: 0 };
    if (Object.keys(to.query).length === 0 || to.query.id) {
      return initPosition;
    } else {
      return to.meta.scrollPosition?.() ?? initPosition;
    }
  }
});

function someRoutesRequireAuth(route) {
  return route.matched.some(record => record.meta?.requiredAuth);
}

router.beforeEach(async (to, from, next) => {
  const token = Cookie.get('token');

  // 頁面需要認證，且沒有 token 時，重新取得 token 後才能進入頁面
  if (someRoutesRequireAuth(to) && !token) {
    await API.refreshToken();
  }
  next();
});

router.afterEach((to, from) => {
  bootstrapGoogleAnalytics({ to, from });
});

export default router;
