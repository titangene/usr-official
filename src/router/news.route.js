import store from '@/store';

import { checkTypeQuery } from '@/utils/route';
import { unpickObjectProps } from '@/utils/helper';

export default [
  {
    path: '/posts',
    name: 'CentralActivities',
    component: () =>
      import(
        /* webpackChunkName: "news" */ '@/views/News/CentralActivities.vue'
      ),
    props(route) {
      return {
        default: true,
        activeYear: route.params?.year,
        activeMonth: route.params?.month
      };
    },
    meta: {
      requiredAuth: true,
      menuTitle: '最新消息',
      breadcrumb: ['最新消息', '中心活動'],
      PageHeaderHeading: 'Event',
      categoryQueryMap: [
        { value: '培力/SIG活動', query: 'sig' },
        { value: '其他活動', query: 'other' },
        { value: 'EXPO', query: 'expo' },
        { value: '線上常設展', query: 'exhibition' }
      ]
    },
    async beforeEnter(to, from, next) {
      if (to.query.keywords) {
        store.commit('activity2/keyword', to.query.keywords);
      }

      const errorQuerys = [];

      checkTypeQuery(
        to,
        function onComplete(type) {
          store.commit('activity2/category', type);
        },
        function onAbort() {
          errorQuerys.push('type');
        }
      );

      if (to.query.time) {
        const year = to.query.time;
        store.commit('activity2/year', Number(year));
      }

      if (errorQuerys.length === 0) {
        if (from.name === 'Home' && to.params.year) {
          store.commit('activity2/year', to.params.year);
        }
        await store.dispatch('activity2/fetchActivities');
        next();
      } else {
        next({
          name: to.name,
          query: unpickObjectProps(to.query, errorQuerys)
        });
      }
    }
  },
  {
    path: '/announce',
    name: 'CentralAnnouncement',
    component: () =>
      import(
        /* webpackChunkName: "news" */ '@/views/News/CentralAnnouncement.vue'
      ),
    meta: {
      requiredAuth: true,
      menuTitle: '最新消息',
      breadcrumb: ['最新消息', '計畫公告'],
      PageHeaderHeading: 'News',
      categoryQueryMap: [
        { value: '最新公告', query: 'news' },
        { value: '修正計畫', query: 'fix' },
        { value: '計畫管考', query: 'appraisal' },
        { value: '成果報告', query: 'report' }
      ]
    },
    async beforeEnter(to, from, next) {
      if (to.query.keywords) {
        store.commit('announce/keyword', to.query.keywords);
      }

      const errorQuerys = [];

      checkTypeQuery(
        to,
        function onComplete(type) {
          store.commit('announce/category', type);
        },
        function onAbort() {
          errorQuerys.push('type');
        }
      );

      if (to.query.time) {
        const year = to.query.time;
        store.commit('announce/year', Number(year));
      }

      if (errorQuerys.length === 0) {
        await store.dispatch('announce/fetchAnnounces');
        next();
      } else {
        next({
          name: to.name,
          query: unpickObjectProps(to.query, errorQuerys)
        });
      }
    }
  },
  {
    path: '/files',
    name: 'Download',
    component: () =>
      import(/* webpackChunkName: "news" */ '@/views/News/Download.vue'),
    meta: {
      requiredAuth: true,
      menuTitle: '最新消息',
      breadcrumb: ['最新消息', '文件下載'],
      PageHeaderHeading: 'Download',
      categoryQueryMap: [
        { value: '計畫公告', query: 'plan' },
        { value: '修正計畫', query: 'fix' },
        { value: '計畫徵件', query: 'solicit' }
      ]
    },
    async beforeEnter(to, from, next) {
      const errorQuerys = [];

      if (to.query.keywords) {
        store.commit('download/keyword', to.query.keywords);
      }

      checkTypeQuery(
        to,
        function onComplete(type) {
          store.commit('download/category', type);
        },
        function onAbort() {
          errorQuerys.push('type');
        }
      );

      if (errorQuerys.length === 0) {
        await store.dispatch('download/fetchDownloadFiles');
        next();
      } else {
        next({
          name: to.name,
          query: unpickObjectProps(to.query, errorQuerys)
        });
      }
    }
  },
  {
    path: '/qa',
    name: 'FAQ',
    component: () =>
      import(/* webpackChunkName: "news" */ '@/views/News/FAQ.vue'),
    meta: {
      requiredAuth: true,
      menuTitle: '最新消息',
      breadcrumb: ['最新消息', 'QA回答'],
      PageHeaderHeading: 'FAQ',
      categoryQueryMap: [
        { value: 'charge', query: 'funds' },
        { value: 'hr', query: 'personnel' },
        { value: 'plan_purpose', query: 'spirit' }
      ]
    },
    async beforeEnter(to, from, next) {
      const errorQuerys = [];

      if (to.query.keywords) {
        store.commit('faq/keyword', to.query.keywords);
      }

      checkTypeQuery(
        to,
        function onComplete(type) {
          store.commit('faq/category', type);
        },
        function onAbort() {
          errorQuerys.push('type');
        }
      );

      if (errorQuerys.length === 0) {
        await store.dispatch('faq/fetchFAQs');
        next();
      } else {
        next({
          name: to.name,
          query: unpickObjectProps(to.query, errorQuerys)
        });
      }
    }
  }
];
