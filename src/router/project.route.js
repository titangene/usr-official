import store from '@/store';
import i18n from '@/plugins/vue-i18n';

export default [
  {
    path: '/calendar',
    name: 'ProjectCalendar',
    component: () =>
      import(
        /* webpackChunkName: "project" */ '@/views/Project/ProjectCalendar.vue'
      ),
    meta: {
      requiredAuth: true,
      menuTitle: '計畫蹲點',
      breadcrumb: ['計畫蹲點', '計畫行事曆'],
      PageHeaderHeading: 'Project Calendar'
    },
    async beforeEnter(to, from, next) {
      store.dispatch('projectCalendar/fetchProjects');
      next();
    }
  },
  {
    path: '/:lang(en)?/map2',
    name: 'PhaseSecondProjectMap',
    component: () =>
      import(
        /* webpackChunkName: "project" */ '@/views/Project/PhaseSecondProjectMap.vue'
      ),
    meta: {
      requiredAuth: true,
      menuTitle: null,
      breadcrumb: [],
      PageHeaderHeading: 'Project Map'
    },
    async beforeEnter(to, from, next) {
      if (to.params.lang === 'en') {
        i18n.locale = 'en';
        to.meta.menuTitle = 'Project Map';
        to.meta.breadcrumb = [
          'Map of USR teams on site',
          'Phase II (2020-2022)'
        ];
      } else {
        to.meta.menuTitle = '計畫蹲點';
        to.meta.breadcrumb = ['計畫蹲點', '第二期計畫地圖'];
      }
      await Promise.all([
        store.dispatch('phaseSecondProjectMap/fetchOptions'),
        store.dispatch('phaseSecondProjectMap/fetchProjectRegionalCount'),
        store.dispatch('phaseSecondProjectMap/fetchProjects')
      ]);
      next();
    }
  },
  {
    path: '/:lang(en)?/map1',
    name: 'PhaseFirstProjectMap',
    component: () =>
      import(
        /* webpackChunkName: "project" */ '@/views/Project/PhaseFirstProjectMap.vue'
      ),
    meta: {
      requiredAuth: true,
      menuTitle: null,
      breadcrumb: [],
      PageHeaderHeading: 'Project Map'
    },
    async beforeEnter(to, from, next) {
      if (to.params.lang === 'en') {
        i18n.locale = 'en';
        to.meta.menuTitle = 'Project Map';
        to.meta.breadcrumb = [
          'Map of USR teams on site',
          'Phase I (2018-2019)'
        ];
      } else {
        to.meta.menuTitle = '計畫蹲點';
        to.meta.breadcrumb = ['計畫蹲點', '第一期計畫地圖'];
      }
      await Promise.all([
        store.dispatch('phaseFirstProjectMap/fetchOptions'),
        store.dispatch('phaseFirstProjectMap/fetchProjectRegionalCount'),
        store.dispatch('phaseFirstProjectMap/fetchProjects')
      ]);
      next();
    }
  }
];
