import store from '@/store';
import i18n from '@/plugins/vue-i18n';

const blogRoutes = [
  {
    path: '/blog',
    name: 'ColumnArticle',
    component: () =>
      import(/* webpackChunkName: "media" */ '@/views/Media/ColumnArticle.vue'),
    meta: {
      requiredAuth: true,
      menuTitle: '專欄與報導',
      breadcrumb: ['專欄與報導', '專欄文章'],
      PageHeaderHeading: 'Blog'
    },
    async beforeEnter(to, from, next) {
      await store.dispatch('blog/fetchBlogs');
      next();
    }
  },
  {
    path: '/blog/:id',
    name: 'ColumnArticleView',
    component: () =>
      import(
        /* webpackChunkName: "media" */ '@/views/Media/ColumnArticleView.vue'
      ),
    meta: {
      requiredAuth: true,
      menuTitle: '專欄與報導',
      breadcrumb: ['專欄與報導', '專欄文章'],
      PageHeaderHeading: 'Blog'
    },
    async beforeEnter(to, from, next) {
      await Promise.all([
        store.dispatch('blog/fetchOtherBlogs'),
        store.dispatch('blog/fetchBlogById', to.params.id)
      ]);

      const blog = store.getters['blog/blog'];

      if (blog) {
        next();
      } else {
        next({ name: 'ColumnArticle' });
      }
    }
  }
];

const mediaReportRoutes = [
  {
    path: '/media',
    name: 'MediaReport',
    component: () =>
      import(/* webpackChunkName: "media" */ '@/views/Media/MediaReport.vue'),
    meta: {
      requiredAuth: true,
      menuTitle: '專欄與報導',
      breadcrumb: ['專欄與報導', '媒體報導'],
      PageHeaderHeading: 'Report'
    },
    async beforeEnter(to, from, next) {
      await store.dispatch('report/fetchReports');
      next();
    }
  },
  {
    path: '/en/media',
    name: 'MediaReportEnglish',
    component: () =>
      import(/* webpackChunkName: "media" */ '@/views/Media/MediaReport.vue'),
    meta: {
      requiredAuth: true,
      menuTitle: 'USR News',
      breadcrumb: ['USR News'],
      PageHeaderHeading: 'Report'
    },
    async beforeEnter(to, from, next) {
      i18n.locale = 'en';
      await store.dispatch('report/fetchReports');
      next();
    }
  },
  {
    path: '/media/:id',
    name: 'MediaReportView',
    component: () =>
      import(
        /* webpackChunkName: "media" */ '@/views/Media/MediaReportView.vue'
      ),
    meta: {
      requiredAuth: true,
      menuTitle: '專欄與報導',
      breadcrumb: ['專欄與報導', '媒體報導'],
      PageHeaderHeading: 'Report'
    },
    async beforeEnter(to, from, next) {
      await Promise.all([
        store.dispatch('report/fetchOtherReports'),
        store.dispatch('report/fetchReportById', to.params.id)
      ]);

      const report = store.getters['report/report'];

      if (report) {
        next();
      } else {
        next({ name: 'MediaReport' });
      }
    }
  },
  {
    path: '/en/media/:id',
    name: 'MediaReportViewEnglish',
    component: () =>
      import(
        /* webpackChunkName: "media" */ '@/views/Media/MediaReportView.vue'
      ),
    meta: {
      requiredAuth: true,
      menuTitle: 'USR News',
      breadcrumb: ['USR News'],
      PageHeaderHeading: 'Report'
    },
    async beforeEnter(to, from, next) {
      i18n.locale = 'en';
      await Promise.all([
        store.dispatch('report/fetchOtherReports'),
        store.dispatch('report/fetchReportById', to.params.id)
      ]);

      const report = store.getters['report/report'];

      if (report) {
        next();
      } else {
        next({ name: 'MediaReport' });
      }
    }
  }
];

export default [...blogRoutes, ...mediaReportRoutes];
