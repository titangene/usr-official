export default [
  {
    path: '/assessment',
    name: 'Assessment',
    components: {
      default: () =>
        import(
          /* webpackChunkName: "assessment" */ '@/views/Assessment/Assessment.vue'
        ),
      afterPageHeader: () =>
        import(
          /* webpackChunkName: "assessment" */ '@/components/Assessment/AssessmentTabs.vue'
        )
    },
    redirect: { name: 'Documents' },
    meta: {
      menuTitle: '效益評估專區',
      breadcrumb: ['效益評估專區'],
      PageHeaderHeading: 'Assessment'
    },
    children: [
      {
        path: 'documents',
        name: 'Documents',
        component: () =>
          import(
            /* webpackChunkName: "assessment" */ '@/views/Assessment/Documents.vue'
          )
      },
      {
        path: 'reports',
        name: 'Reports',
        component: () =>
          import(
            /* webpackChunkName: "assessment" */ '@/views/Assessment/Reports.vue'
          )
      },
      {
        path: 'workshops',
        name: 'Workshops',
        component: () =>
          import(
            /* webpackChunkName: "assessment" */ '@/views/Assessment/Workshops.vue'
          )
      },
      {
        path: 'tool-box',
        name: 'ToolBox',
        component: () =>
          import(
            /* webpackChunkName: "assessment" */ '@/views/Assessment/ToolBox.vue'
          )
      },
      {
        path: 'related-projects',
        name: 'RelatedProjects',
        component: () =>
          import(
            /* webpackChunkName: "assessment" */ '@/views/Assessment/RelatedProjects.vue'
          )
      },
      {
        path: 'international-projects',
        name: 'InternationalProjects',
        component: () =>
          import(
            /* webpackChunkName: "assessment" */ '@/views/Assessment/InternationalProjects.vue'
          )
      }
    ]
  }
];
