import store from '@/store';
import i18n from '@/plugins/vue-i18n';

import Home from '@/views/Home.vue';
import HomeBanner from '@/components/Home/HomeBanner.vue';
import HomeEn from '@/views/Home-en.vue';
import HomeBannerEN from '@/components/Home/HomeBannerEN.vue';
import Login from '@/views/Login.vue';
import Contact from '@/views/Contact.vue';

export default [
  {
    path: '/',
    name: 'Home',
    components: {
      beforePageHeader: HomeBanner,
      default: Home
    },
    meta: { requiredAuth: true, menuTitle: '首頁' },
    async beforeEnter(to, from, next) {
      await Promise.all([
        store.dispatch('home/fetchNews'),
        store.dispatch('home/fetchBlogs'),
        store.dispatch('home/fetchActivities'),
        store.dispatch('home/fetchVideos')
      ]);
      next();
    }
  },
  {
    path: '/en',
    name: 'HomeEnglish',
    components: {
      beforePageHeader: HomeBannerEN,
      default: HomeEn
    },
    meta: { requiredAuth: true, menuTitle: 'Home' },
    async beforeEnter(to, from, next) {
      i18n.locale = 'en';
      await Promise.all([
        store.dispatch('home/fetchReports'),
        store.dispatch('home/fetchVideos')
      ]);
      next();
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { menuTitle: '學校計畫登入' }
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact,
    meta: { requiredAuth: true, menuTitle: '聯絡我們' }
  }
];
