import store from '@/store';

import Site from '@/views/Site/Site.vue';

export default [
  {
    path: '/site',
    name: 'Site',
    component: Site,
    meta: {
      requiredAuth: true,
      menuTitle: '友站專區',
      breadcrumb: ['友站專區'],
      PageHeaderHeading: 'Site'
    },
    async beforeEnter(to, from, next) {
      await store.dispatch('site/fetchOtherActivities');
      next();
    }
  }
];
