import store from '@/store';
import i18n from '@/plugins/vue-i18n';

const aboutUSRRoutes = [
  {
    path: '/about-1',
    name: 'AboutUSR',
    component: () =>
      import(/* webpackChunkName: "about" */ '@/views/About/AboutUSR.vue'),
    meta: {
      requiredAuth: true,
      menuTitle: '關於USR',
      breadcrumb: ['關於USR', '什麼是USR'],
      PageHeaderHeading: 'About'
    }
  },
  {
    path: '/en/about-1',
    name: 'AboutUSREnglish',
    component: () =>
      import(/* webpackChunkName: "about" */ '@/views/About/AboutUSR.vue'),
    meta: {
      requiredAuth: true,
      menuTitle: 'About USR',
      breadcrumb: ['About USR', 'Introduction'],
      PageHeaderHeading: 'About'
    },
    async beforeEnter(to, from, next) {
      i18n.locale = 'en';
      next();
    }
  }
];

const organizationRoutes = [
  {
    path: '/about-2',
    name: 'Organization',
    component: () =>
      import(/* webpackChunkName: "about" */ '@/views/About/Organization.vue'),
    meta: {
      requiredAuth: true,
      menuTitle: '關於USR',
      breadcrumb: ['關於USR', '組織架構'],
      PageHeaderHeading: 'Team'
    },
    async beforeEnter(to, from, next) {
      await store.dispatch('organization/fetchOrganization');
      next();
    }
  },
  {
    path: '/en/about-2',
    name: 'OrganizationEnglish',
    component: () =>
      import(/* webpackChunkName: "about" */ '@/views/About/Organization.vue'),
    meta: {
      requiredAuth: true,
      menuTitle: 'About USR',
      breadcrumb: ['About USR', 'Organizational Chart'],
      PageHeaderHeading: 'Team'
    },
    async beforeEnter(to, from, next) {
      i18n.locale = 'en';
      await store.dispatch('organization/fetchOrganization');
      next();
    }
  }
];

export default [...aboutUSRRoutes, ...organizationRoutes];
