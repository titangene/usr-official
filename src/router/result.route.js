import store from '@/store';

export default [
  {
    path: '/ebooks',
    name: 'EBook',
    component: () =>
      import(/* webpackChunkName: "result" */ '@/views/Result/EBook.vue'),
    meta: {
      requiredAuth: true,
      menuTitle: '品味USR',
      breadcrumb: ['品味USR', '電子書'],
      PageHeaderHeading: 'E-Book'
    },
    async beforeEnter(to, from, next) {
      await store.dispatch('eBook/fetchEBooks');
      next();
    }
  },
  {
    path: '/exhibition/(result)?',
    name: 'Exhibition',
    components: {
      beforePageHeader: () =>
        import(
          /* webpackChunkName: "result" */ '@/components/Exhibition/ExhibitionPrimaryVideo.vue'
        ),
      default: () =>
        import(/* webpackChunkName: "result" */ '@/views/Result/Exhibition.vue')
    },
    meta: {
      requiredAuth: true,
      menuTitle: '品味USR',
      breadcrumb: ['品味USR', '線上常設展'],
      PageHeaderHeading: 'Exhibition',
      scrollPosition() {
        const headerHeight = document.querySelector('.app > header')
          .clientHeight;
        const bannerHeight =
          document.querySelector('.Exhibition-banner')?.clientHeight ?? 0;
        const scrollY = headerHeight + bannerHeight + 160;

        if (store.getters['exhibition/introduction']) {
          return { x: 0, y: scrollY };
        } else {
          return { x: 0, y: 0 };
        }
      }
    }
  },
  {
    path: '/exhibition/:id',
    name: 'ExhibitionColumn',
    component: () =>
      import(
        /* webpackChunkName: "result" */ '@/views/Result/ExhibitionColumn.vue'
      ),
    meta: {
      requiredAuth: true,
      menuTitle: '品味USR',
      breadcrumb: ['品味USR', '線上常設展', '專欄內容'],
      PageHeaderHeading: 'Exhibition'
    },
    async beforeEnter(to, from, next) {
      await Promise.all([
        // pageSize 設為 5 是因為要用於相關文章，共有 4 篇相關文章，但不包含當前文章
        store.dispatch('exhibition/fetchExhibitions', { pageSize: 5 }),
        store.dispatch('exhibition/fetchExhibitionById', to.params.id)
      ]);

      const exhibition = store.getters['exhibition/exhibition'];

      if (exhibition) {
        next();
      } else {
        next({ name: 'Exhibition' });
      }
    }
  }
];
