import API from '@/utils/BackendAPI';

export default {
  namespaced: true,
  state: {
    books: [],
    activeBook: {
      id: null,
      title: null,
      cover: null,
      books: []
    }
  },
  getters: {
    books(state) {
      return state.books;
    },
    activeBook(state, getters) {
      return state.activeBook ?? getters.initActiveBook;
    },
    initActiveBook() {
      return {
        id: null,
        title: null,
        cover: null,
        books: []
      };
    }
  },
  mutations: {
    books(state, payload) {
      state.books = [...payload];
    },
    activeBook(state, payload) {
      state.activeBook = payload;
    }
  },
  actions: {
    async fetchEBooks({ commit }) {
      const books = await API.POST('emp/read.php', {
        function: 'get_e_book_data'
      });
      commit('books', books);
    },

    async fetchEBookById({ commit }, eBookId) {
      const book = await API.POST('emp/read.php', {
        function: 'get_e_book_data',
        id: eBookId
      });

      const { id, title, cover, page_link, books } = book;
      const activeBook = {
        ...{ id, title, cover },
        books: books.map(page => page_link + page)
      };

      commit('activeBook', activeBook);
    }
  }
};

// 重構後端回傳的春芽電子書,將資料取出
// function restructureEBook(book) {
//   const { id, title, pdf, cover } = book;
//   return { id, title, pdf, cover };
// }
