import API from '@/utils/BackendAPI';

export default {
  namespaced: true,
  state: {
    form: {
      identity: null,
      class: null,
      name: '',
      company: '',
      phone: '',
      email: '',
      contents: '',
      g_recaptcha_response: ''
    }
  },
  getters: {
    form(state) {
      return state.form;
    }
  },
  mutations: {
    form(state, payload) {
      state.form = { ...payload };
    }
  },
  actions: {
    async submitForm({ getters, commit }, payload) {
      commit('form', payload);
      const { message } = await API.POST('/emp/read.php', {
        function: 'mail',
        ...getters.form
      });
      return message;
    }
  }
};
