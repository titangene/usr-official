import { orderBy } from 'lodash-es';

import API from '@/utils/BackendAPI';
import { formatDate } from '@/utils/format';
import { getMonth, getYear, sortDate } from '@/utils/dateHelper';
import { groupByToArray } from '@/utils/helper';

export default {
  namespaced: true,
  state: {
    year: getYear(new Date()),
    category: null,
    keyword: '',

    activities: [],

    activeActivityId: null,
    activeActivity: null
  },
  getters: {
    year(state) {
      return state.year;
    },
    category(state) {
      return state.category;
    },
    keyword(state) {
      return state.keyword;
    },
    activities(state) {
      return state.activities;
    },
    activitiesByMonthGroup(state, getters) {
      const activityGroup = groupByToArray(
        sortDate(getters.activities),
        { keyName: 'month', valueName: 'activities' },
        item => {
          const date = item.date;
          return getYear(date) === getters.year ? getMonth(date) : null;
        }
      );
      return orderBy(activityGroup, item => Number(item.month), 'desc');
    },

    activeActivityId(state) {
      return state.activeActivityId;
    },
    activeActivity(state) {
      return state.activeActivity;
    }
  },
  mutations: {
    year(state, activities) {
      state.year = activities;
    },
    category(state, payload) {
      state.category = payload;
    },
    keyword(state, payload) {
      state.keyword = payload;
    },

    activities(state, activities) {
      state.activities = activities
        .map(restructureActivity)
        .flatMap(generateSchedule);
    },

    activeActivityId(state, payload) {
      state.activeActivityId = payload;
    },
    activeActivity(state, payload) {
      state.activeActivity = payload ? { ...payload } : null;
    }
  },
  actions: {
    async fetchActivities({ commit, getters }) {
      const { data: activities } = await API.POST('emp/read.php', {
        function: 'USR_calendar',
        years: getters.year ?? '',
        class_name: getters.category ?? '',
        key_word: getters.keyword
      });
      commit('activities', activities);
    },
    async fetchActivityById({ commit }, activityId) {
      const { data: activity } = await API.POST('emp/read.php', {
        function: 'USR_calendar',
        id: activityId
      });

      commit('activeActivity', restructureActivity(activity));
    }
  }
};

// 重構後端回傳的中心活動，將資料取出
function restructureActivity(activity) {
  const {
    id,
    title,
    class_name: category,
    picture: image,
    contents,
    opendate: _opendate,
    closedate: _closedate
  } = activity;

  const opendate = formatDate(_opendate);
  const closedate = _closedate ? formatDate(_closedate) : null;
  const schedule = closedate ? `${opendate} ~ ${closedate}` : opendate;

  return {
    id,
    title,
    category,
    image,
    contents,
    opendate,
    closedate,
    schedule
  };
}

function generateSchedule(activity) {
  return generateStartAndEndSchedule(activity, {
    startNote: '(活動發佈)',
    endNote: '(活動結束)'
  });
}

function generateStartAndEndSchedule(activity, { startNote, endNote }) {
  const startSchedule = {
    ...activity,
    date: activity.opendate,
    note: activity.closedate ? startNote : ''
  };

  const endSchedule = activity.closedate
    ? {
        ...activity,
        date: activity.closedate,
        note: endNote
      }
    : null;

  return endSchedule ? [startSchedule, endSchedule] : [startSchedule];
}
