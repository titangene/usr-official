import { debounce } from 'lodash-es';

import API from '@/utils/BackendAPI';
import { formatDate } from '@/utils/format';

export default {
  namespaced: true,
  state: {
    year: null,
    category: null,
    keyword: '',

    announces: [],
    total: 0,

    activeAnnounceId: null,
    activeAnnounce: null
  },
  getters: {
    year(state) {
      return state.year;
    },
    category(state) {
      return state.category;
    },
    keyword(state) {
      return state.keyword;
    },

    announces(state) {
      return state.announces;
    },
    total(state) {
      return state.total;
    },

    activeAnnounceId(state) {
      return state.activeAnnounceId;
    },
    activeAnnounce(state) {
      return state.activeAnnounce;
    }
  },
  mutations: {
    year(state, payload) {
      state.year = payload;
    },
    category(state, payload) {
      state.category = payload;
    },
    keyword(state, payload) {
      state.keyword = payload;
    },

    announces(state, announces) {
      state.announces = announces.map(restructureAnnounce);
    },
    total(state, payload) {
      state.total = payload;
    },

    activeAnnounceId(state, payload) {
      state.activeAnnounceId = payload;
    },
    activeAnnounce(state, payload) {
      state.activeAnnounce = payload ? { ...payload } : null;
    }
  },
  actions: {
    fetchAnnouncesDebounce: debounce(({ dispatch }, pageOptions) => {
      dispatch('fetchAnnounces', pageOptions);
    }, 400),
    async fetchAnnounceById({ commit }, announceId) {
      const { data: announce } = await API.POST('emp/read.php', {
        function: 'news',
        type: 'sop',
        id: announceId
      });

      commit('activeAnnounce', restructureAnnounce(announce));
    },
    async fetchAnnounces({ commit, getters }, { currentPage, pageSize } = {}) {
      const { total, data: announces } = await API.POST('emp/read.php', {
        function: 'news',
        type: 'sop',
        years: getters.year ?? '',
        class_name: getters.category ?? '',
        key_word: getters.keyword,
        page: currentPage ?? 1,
        page_num: pageSize ?? 10
      });

      commit('total', total);
      commit('announces', announces);
    }
  }
};

function restructureAnnounce(announce) {
  const {
    type: _type,
    opendate,
    class_name: category,
    picture: cover,
    ...otherProps
  } = announce;

  return {
    cover,
    category,
    opendate: formatDate(opendate),
    ...otherProps
  };
}
