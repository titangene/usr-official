import { flow, debounce } from 'lodash-es';
import { startOfMonth } from 'date-fns/esm//fp';

import API from '@/utils/BackendAPI';
import { _formatDate, parseDateTimeAndFormatDate } from '@/utils/format.js';
import { sortDate, getMonth, getLastDayOfNextMonth } from '@/utils/dateHelper';

export default {
  namespaced: true,
  state: {
    fromDate: null,
    toDate: null,
    category: null,
    keyword: '',
    clickActivityId: null,
    activity: null,
    list: {
      firstMonth: [],
      secondMonth: []
    },
    calendar: {
      firstMonth: [],
      secondMonth: []
    }
  },
  getters: {
    initFromDate() {
      return flow(startOfMonth, _formatDate)(new Date());
    },
    initToDate() {
      return flow(getLastDayOfNextMonth, _formatDate)(new Date());
    },
    fromDate(state, getters) {
      return state.fromDate || getters.initFromDate;
    },
    toDate(state, getters) {
      return state.toDate || getters.initToDate;
    },
    category(state) {
      return state.category;
    },
    keyword(state) {
      return state.keyword;
    },
    clickActivityId(state) {
      return state.clickActivityId;
    },
    activity(state, getters) {
      return state.activity ?? getters.activityById;
    },
    activityById(state, getters) {
      return Object.values(getters.calendar)
        .flat()
        .find(activity => activity.id === getters.clickActivityId);
    },
    list(state) {
      return state.list;
    },
    calendar(state) {
      return state.calendar;
    },
    isNoActivity(state, getters) {
      return Object.values(getters.list).flat().length === 0;
    }
  },
  mutations: {
    fromDate(state, payload) {
      state.fromDate = payload;
    },
    toDate(state, payload) {
      state.toDate = payload;
    },
    category(state, payload) {
      state.category = payload;
    },
    keyword(state, payload) {
      state.keyword = payload;
    },
    clickActivityId(state, payload) {
      state.clickActivityId = payload;
    },
    activity(state, payload) {
      state.activity = payload ? { ...payload } : null;
    },
    list(state, payload) {
      state.list = { ...payload };
    },
    calendar(state, payload) {
      state.calendar = { ...payload };
    }
  },
  actions: {
    fetchActivitiesDebounce: debounce(({ dispatch }, options = {}) => {
      if (options?.updateCalendar) dispatch('fetchActivities');
      dispatch('fetchFilteredActivities');
    }, 400),
    async fetchNearlyTwoMonthsActivities({ dispatch }) {
      Promise.all([
        dispatch('fetchActivities'),
        dispatch('fetchFilteredActivities')
      ]);
    },
    async fetchActivityById({ commit }, activityId) {
      const {
        data: [activity]
      } = await API.POST('emp/read.php', {
        function: 'activity_calendar',
        id: activityId
      });

      commit('activity', restructureActivity(activity));
    },
    async fetchActivities({ commit, getters }) {
      const { data: activities } = await API.POST('emp/read.php', {
        function: 'activity_calendar',
        date_from: getters.fromDate,
        date_to: getters.toDate
      });

      const groupByMonth = getActivitiesGroupByMonth(
        activities,
        getters.fromDate
      );

      commit('calendar', groupByMonth);
    },
    async fetchFilteredActivities({ commit, getters }) {
      const { data: activities } = await API.POST('emp/read.php', {
        function: 'activity_calendar',
        date_from: getters.fromDate,
        date_to: getters.toDate,
        class_name: getters.category ?? '',
        key_word: getters.keyword
      });

      const groupByMonth = getActivitiesGroupByMonth(
        activities,
        getters.fromDate
      );

      commit('list', groupByMonth);
    }
  }
};

function getActivitiesGroupByMonth(activities, fromDate) {
  const { firstMonth, secondMonth } = activities
    .map(restructureActivity)
    .flatMap(generateSchedule)
    .reduce(
      (groupByMonth, activity) => {
        const monthOfActivityDate = getMonth(activity.date);
        const monthOfFromDate = getMonth(fromDate);
        const monthOfToDate = monthOfFromDate + 1;

        if (monthOfActivityDate === monthOfFromDate) {
          return {
            ...groupByMonth,
            firstMonth: [...groupByMonth.firstMonth, activity]
          };
        }

        if (monthOfActivityDate === monthOfToDate) {
          return {
            ...groupByMonth,
            secondMonth: [...groupByMonth.secondMonth, activity]
          };
        }

        return groupByMonth;
      },
      { firstMonth: [], secondMonth: [] }
    );

  return {
    firstMonth: sortDate(firstMonth),
    secondMonth: sortDate(secondMonth)
  };
}

/**
 * @param {Object} activity
 * @returns {Array} `activity[]`
 */
function generateSchedule(activity) {
  if (activity.tag === 'activity') {
    return generateScheduleByActivity(activity);
  } else if (activity.tag === 'announcement') {
    return generateScheduleByAnnouncement(activity);
  } else {
    return generateScheduleByOther(activity);
  }
}

/**
 * @description 活動分類的行程有 opendate 至 closedate，需生成這範圍內的行程
 * @example `opendate` 為 '2020-01-01'，`closedate` 為 '2020-01-05'，
 * 需生成 `2020-01-01` 至 `2020-01-05` 的行程
 */
function generateScheduleByActivity(activity) {
  return generateStartAndEndSchedule(activity, {
    startNote: '(活動發佈)',
    endNote: '(活動結束)'
  });
}

/**
 * @description
 * - 公告分類的行程只須生成 opendate 和 closedate 的行程，
 *   且在 opendate 的行程 date 後面加上 "(公告發佈)"，
 *   以及在 closedate 的行程 date 後面加上 "(收件截止)"。
 * - 若沒有 closedate，opendate 的行程不用加上 "(公告發佈)"
 */
function generateScheduleByAnnouncement(activity) {
  return generateStartAndEndSchedule(activity, {
    startNote: '(公告發佈)',
    endNote: '(收件截止)'
  });
}

/**
 * @description 其他分類的行程
 * - 回傳值必須為陣列，所以多用陣列包起來
 */
function generateScheduleByOther(activity) {
  return [
    {
      ...activity,
      vueKey: activity.id,
      date: activity.opendate
    }
  ];
}

function generateStartAndEndSchedule(activity, { startNote, endNote }) {
  const startSchedule = {
    ...activity,
    vueKey: `${activity.id}_${activity.opendate}`,
    date: activity.opendate,
    note: activity.closedate ? startNote : ''
  };

  const endSchedule = activity.closedate
    ? {
        ...activity,
        vueKey: `${activity.id}-${activity.closedate}`,
        date: activity.closedate,
        note: endNote
      }
    : null;

  return endSchedule ? [startSchedule, endSchedule] : [startSchedule];
}

function restructureActivity(activity) {
  const {
    class_name: category,
    picture: image,
    opendate,
    closedate,
    ...otherProps
  } = activity;
  const tag = getTag(activity);

  return {
    image,
    category,
    tag,
    opendate: parseDateTimeAndFormatDate(opendate),
    closedate: closedate ? parseDateTimeAndFormatDate(closedate) : null,
    ...otherProps
  };
}

function getTag(activity) {
  const activityTags = ['培力/SIG活動', '其他活動', 'EXPO', '線上常設展'];
  const announcementTags = ['最新公告', '修正計畫', '計畫管考', '成果報告'];

  if (activityTags.includes(activity.class_name)) return 'activity';
  if (announcementTags.includes(activity.class_name)) return 'announcement';
  return 'other';
}

const generateActivitySchedule = generateSchedule;

export { restructureActivity, generateActivitySchedule };
