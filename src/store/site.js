import API from '@/utils/BackendAPI';
import { formatDate, limitTextCountInHTML } from '@/utils/format';

export default {
  namespaced: true,
  state: {
    otherActivities: [],
    activeOtherActivityId: null,
    activeOtherActivity: null
  },
  getters: {
    otherActivities(state) {
      return state.otherActivities;
    },
    activeOtherActivityId(state) {
      return state.activeOtherActivityId;
    },
    activeOtherActivity(state) {
      return state.activeOtherActivity;
    }
  },
  mutations: {
    otherActivities(state, otherActivities) {
      state.otherActivities = otherActivities.map(otherActivity => {
        return {
          ...otherActivity,
          opendate: formatDate(otherActivity.opendate),
          description: limitTextCountInHTML(otherActivity.description, 30)
        };
      });
    },
    activeOtherActivityId(state, payload) {
      state.activeOtherActivityId = payload;
    },
    activeOtherActivity(state, payload) {
      state.activeOtherActivity = payload ? { ...payload } : null;
    }
  },
  actions: {
    async fetchOtherActivitiesById({ commit, getters }) {
      const { data: otherActivity } = await API.POST('emp/read.php', {
        function: 'other_activities',
        id: getters.activeOtherActivityId
      });

      commit('activeOtherActivity', otherActivity);
    },
    async fetchOtherActivities({ commit }) {
      const { data: otherActivities } = await API.POST('emp/read.php', {
        function: 'other_activities'
      });

      commit('otherActivities', otherActivities);
    }
  }
};
