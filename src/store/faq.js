import API from '@/utils/BackendAPI';

export default {
  namespaced: true,
  state: {
    category: null,
    keyword: '',
    FAQs: [],
    total: 0,
    form: {
      identity: null,
      class: null,
      name: '',
      company: '',
      phone: '',
      email: '',
      contents: '',
      g_recaptcha_response: ''
    }
  },
  getters: {
    category(state) {
      return state.category;
    },
    keyword(state) {
      return state.keyword;
    },
    FAQs(state) {
      return state.FAQs;
    },
    total(state) {
      return state.total;
    },
    form(state) {
      return state.form;
    }
  },
  mutations: {
    category(state, payload) {
      state.category = payload;
    },
    keyword(state, payload) {
      state.keyword = payload;
    },
    FAQs(state, payload) {
      state.FAQs = [...payload];
    },
    total(state, payload) {
      state.total = payload;
    },
    form(state, payload) {
      state.form = { ...payload };
    }
  },
  actions: {
    async fetchFAQs({ commit, getters }, { currentPage, pageSize } = {}) {
      const { total, data: FAQs } = await API.POST('emp/read.php', {
        function: 'questions',
        type: getters.category ?? '',
        title: getters.keyword,
        page: currentPage ?? 1,
        page_num: pageSize ?? 10
      });

      commit('FAQs', FAQs);
      commit('total', total);
    },
    async submitForm({ getters, commit }, payload) {
      commit('form', payload);
      const { message } = await API.POST('/emp/read.php', {
        function: 'ask_questions',
        ...getters.form
      });
      return message;
    }
  }
};
