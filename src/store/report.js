import { debounce } from 'lodash-es';

import i18n from '@/plugins/vue-i18n';
import API from '@/utils/BackendAPI';
import {
  formatDate,
  limitTextCountInHTML,
  limitEnglishWordCount,
  getUrlWordBreakHTML
} from '@/utils/format';

export default {
  namespaced: true,
  state: {
    year: null,
    keyword: '',
    topReport: {
      image: null,
      date: null,
      title: '',
      description: ''
    },
    reports: [],
    otherReports: [],
    report: null,
    total: 0
  },
  getters: {
    year(state) {
      return state.year;
    },
    keyword(state) {
      return state.keyword;
    },
    topReport(state) {
      return state.topReport;
    },
    reports(state) {
      return state.reports;
    },
    otherReports(state, getters) {
      return state.otherReports
        .filter(report => report.id !== getters.report.id)
        .slice(0, 4);
    },
    report(state) {
      return state.report;
    },
    total(state) {
      return state.total;
    }
  },
  mutations: {
    year(state, payload) {
      state.year = payload;
    },
    keyword(state, payload) {
      state.keyword = payload;
    },
    topReport(state, report) {
      state.topReport = restructureReportTop(report);
    },
    reports(state, reports) {
      state.reports = reports.map(restructureReport);
    },
    otherReports(state, otherReports) {
      state.otherReports = otherReports.map(restructureReport).map(item => {
        const { title } = item;
        return {
          ...item,
          title: i18n.locale === 'en' ? limitEnglishWordCount(title, 10) : title
        };
      });
    },
    report(state, report) {
      // const report = { ...payload };
      // const UrlPattern = /(?:http|https):\/\/(?:[\w-]+)(?:\.[\w-]+)+(?:[\w.,@?^=%&amp;:/~+#-]*[\w@?^=%&amp;/~+#-])?/;

      // let contents = report.contents.replace(UrlPattern, function replacer(match) {
      //   return `<span style="word-break:break-all">${match}</span>`;
      // });
      // const reports = {contents: contents, ...report}
      state.report = restructureReport(report);
    },
    total(state, payload) {
      state.total = payload;
    }
  },
  actions: {
    fetchReportsDebounce: debounce(({ dispatch }, pageOptions) => {
      dispatch('fetchReports', pageOptions);
    }, 400),
    async fetchReportById({ commit }, reportId) {
      const { data: report } = await API.POST('emp/read.php', {
        function: 'media_report',
        language: i18n.locale,
        id: reportId
      });

      if (report) {
        commit('report', report);
      }
    },
    async fetchReports({ commit, getters }, { currentPage, pageSize } = {}) {
      const { total, data: reports, top: topReport } = await API.POST(
        'emp/read.php',
        {
          function: 'media_report',
          language: i18n.locale,
          years: getters.year ?? '',
          title: getters.keyword,
          page: currentPage ?? 1,
          page_num: pageSize ?? 10
        }
      );

      commit('total', total);
      commit('reports', reports);
      commit('topReport', topReport);
    },
    async fetchOtherReports({ commit }) {
      const { data: otherReports } = await API.POST('emp/read.php', {
        function: 'media_report',
        language: i18n.locale,
        page: 1,
        page_num: 10
      });
      commit('otherReports', otherReports);
    }
  }
};

function restructureReportTop(report) {
  const { description, time, Small_picture: image, ...otherProps } = report;

  return {
    image,
    date: formatDate(time),
    description:
      i18n.locale === 'en'
        ? description
        : limitTextCountInHTML(description, 170),
    ...otherProps
  };
}

function restructureReport(report) {
  const { time, Small_picture: image, contents, ...otherProps } = report;

  return {
    image,
    contents: contents ? getUrlWordBreakHTML(contents) : null,
    date: formatDate(time),
    ...otherProps
  };
}
