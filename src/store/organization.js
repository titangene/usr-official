import i18n from '@/plugins/vue-i18n';
import { groupBy } from '@/utils/helper';
import { OrganizationAPI } from '@/api';
// import API from '@/utils/BackendAPI';

export default {
  namespaced: true,
  state: {
    organization: {}
  },
  getters: {
    organization(state) {
      return state.organization;
    }
  },
  mutations: {
    organization(state, payload) {
      state.organization = { ...payload };
    }
  },
  actions: {
    async fetchOrganization({ commit }) {
      const members = await OrganizationAPI[i18n.locale].list();

      // 舊 API
      // const { data: members } = await API.POST('/emp/read.php', {
      //   function: 'organization',
      //   language: i18n.locale
      // });

      const projectJob = {
        investigator: i18n.t('Organization.projectJob.investigator'),
        convener: i18n.t('Organization.projectJob.convener')
      };
      const section = {
        綜合業務組: i18n.t('Organization.section.comprehensiveBusiness'),
        評估企劃組: i18n.t('Organization.section.evalutionAndPlanning'),
        諮詢協作組: i18n.t('Organization.section.counseling')
      };

      const organization = groupBy(members, member => {
        return member.project_job?.includes(projectJob.investigator)
          ? 'investigators'
          : 'other';
      });

      const investigators = organization.investigators;
      const other = groupBy(organization.other, 'group_name');

      const counselingSection = other[section['諮詢協作組']].map(member => {
        if (i18n.locale === 'en') {
          member.district = member.district_name.replaceAll('/', `/<wbr>`);
        } else {
          member.district = member.district_name;
        }
        return member;
      });

      const advisoryCooperationGroup = groupBy(counselingSection, member =>
        member.job_title.includes(projectJob.convener)
          ? projectJob.convener
          : member.job_title
      );

      commit('organization', {
        investigators,
        other: {
          綜合業務組: other[section['綜合業務組']],
          評估企劃組: other[section['評估企劃組']],
          諮詢協作組: advisoryCooperationGroup
        }
      });
    }
  }
};
