import API from '@/utils/BackendAPI';
import {
  formatDate,
  limitTextCountInHTML,
  limitEnglishWordCount
} from '@/utils/format';
import { getYouTubeVideoId } from '@/utils/helper';
import {
  restructureActivity,
  generateActivitySchedule
} from '@/store/activity';

const Videos = {
  state: {
    videos: []
  },
  getters: {
    videos(state) {
      return state.videos;
    }
  },
  mutations: {
    videos(state, videos) {
      state.videos = videos.map(video => {
        return {
          title: video.title,
          youTubeVideoId: getYouTubeVideoId(video.video_url)
        };
      });
    }
  },
  actions: {
    async fetchVideos({ commit }) {
      const { data: videos } = await API.POST('/emp/read.php', {
        function: 'get_homepage_video_data'
      });

      commit('videos', videos);
    }
  }
};

export default {
  namespaced: true,
  state: {
    carouselItems: [],
    topics: [],
    blogs: [],
    reports: [],
    activities: [],
    ...Videos.state
  },
  getters: {
    carouselItems(state) {
      return state.carouselItems.reverse();
    },
    topics(state) {
      return state.topics.reverse();
    },
    blogs(state) {
      return state.blogs;
    },
    reports(state) {
      return state.reports;
    },
    activities(state) {
      return state.activities;
    },
    ...Videos.getters
  },
  mutations: {
    carouselItems(state, carouselItems) {
      state.carouselItems = carouselItems.map(blog => {
        const {
          // eslint-disable-next-line no-unused-vars
          type,
          description,
          opendate,
          closedate,
          Home_picture: cover,
          ...otherProps
        } = blog;

        const postType = getPostType(blog);
        const { link, type: linkType } = determineLinkType(blog, postType);

        return {
          cover,
          type: postType,
          link,
          linkType,
          opendate: formatDate(opendate),
          closedate: closedate ? formatDate(closedate) : null,
          description: limitTextCountInHTML(description, 30),
          ...otherProps
        };
      });
    },
    topics(state, topics) {
      state.topics = topics.map(blog => {
        const {
          // eslint-disable-next-line no-unused-vars
          type,
          // eslint-disable-next-line no-unused-vars
          contents,
          opendate,
          closedate,
          Small_picture: image,
          ...otherProps
        } = blog;

        const postType = getPostType(blog);
        const { link, type: linkType } = determineLinkType(blog, postType);

        return {
          image,
          type: postType,
          link,
          linkType,
          opendate: formatDate(opendate),
          closedate: closedate ? formatDate(closedate) : null,
          ...otherProps
        };
      });
    },
    blogs(state, blogs) {
      state.blogs = blogs.map(blog => {
        // eslint-disable-next-line no-unused-vars
        const { contents, time, Small_picture: image, ...otherProps } = blog;
        return {
          image,
          time: formatDate(time),
          ...otherProps
        };
      });
    },
    reports(state, reports) {
      state.reports = reports.map(report => {
        // eslint-disable-next-line no-unused-vars
        const { title, time, Small_picture: image, ...otherProps } = report;
        return {
          image,
          title: limitEnglishWordCount(title, 10),
          time: formatDate(time),
          ...otherProps
        };
      });
    },
    activities(state, activities) {
      state.activities = activities.map(activity => {
        const {
          // eslint-disable-next-line no-unused-vars
          contents,
          opendate: date,
          picture: image,
          ...otherProps
        } = activity;
        return {
          image,
          date: formatDate(date),
          ...otherProps
        };
      });
    },
    ...Videos.mutations
  },
  actions: {
    async fetchNews({ commit }) {
      const { data: news } = await API.POST('/emp/read.php', {
        function: 'Homepage_news'
      });

      const newsGroup = news.reduce(
        (group, item) => {
          if (item.Home_index) {
            const carouselItemIndex = Number(item.Home_index) - 1;
            group.carouselItems[carouselItemIndex] = item;
          }

          if (item.Small_index) {
            const topicIndex = Number(item.Small_index) - 1;
            group.topics[topicIndex] = item;
          }

          return {
            carouselItems: [...group.carouselItems],
            topics: [...group.topics]
          };
        },
        { carouselItems: [], topics: [] }
      );
      commit('carouselItems', newsGroup.carouselItems);
      commit('topics', newsGroup.topics);
    },
    async fetchBlogs({ commit }) {
      const { data: blogs } = await API.POST('emp/read.php', {
        function: 'usr_blog',
        page_num: '6'
      });
      commit('blogs', blogs);
    },
    async fetchReports({ commit }) {
      const { data: report } = await API.POST('emp/read.php', {
        function: 'media_report',
        language: 'en',
        page_num: '6'
      });
      commit('reports', report);
    },
    async fetchActivities({ commit }) {
      const { data: activities } = await API.POST('emp/read.php', {
        function: 'activity_calendar'
      });
      commit(
        'activities',
        activities.map(restructureActivity).flatMap(generateActivitySchedule)
      );
    },
    ...Videos.actions
  }
};

function getPostType(post) {
  if (post.external_url !== null) return 'externalLink';
  if (post.type.includes('exhibition')) return 'exhibition';
  return post.type;
}

/** 判斷連結類型 */
function determineLinkType(post, postType) {
  const id = post.id;
  switch (postType) {
    case 'externalLink':
      return {
        type: 'external',
        link: post.external_url
      };
    case 'activity':
      return {
        type: 'internal',
        link: { name: 'CentralActivities', query: { id } }
      };
    case 'sop':
      return {
        type: 'internal',
        link: { name: 'CentralAnnouncement', query: { id } }
      };
    case 'exhibition':
      return {
        type: 'internal',
        link: { name: 'ExhibitionColumn', params: { id } }
      };
    case 'blog':
      return {
        type: 'internal',
        link: { name: 'ColumnArticleView', params: { id } }
      };
    case 'media':
      return {
        type: 'internal',
        link: { name: 'MediaReportView', params: { id } }
      };

    // 若為其他 type (即後端提供錯誤的 type) 時，將 type 視為其他，且不提供連結
    default:
      return {
        type: 'other',
        link: null
      };
  }
}
