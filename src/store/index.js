import Vue from 'vue';
import Vuex from 'vuex';

import Home from './Home';
import Login from './login';
import Contact from './contact';
import Organization from './organization';
import Activity from './activity';
import Activity2 from './activity-2';
import Announce from './announce';
import Download from './download';
import FAQ from './faq';
import EBook from './e-book';
import Exhibition from './exhibition';
import ProjectCalendar from './project-calendar';
import PhaseSecondProjectMap from './phase-second-project-map';
import PhaseFirstProjectMap from './phase-first-project-map';
import Blog from './blog';
import Report from './report';
import Site from './site';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    home: Home,
    login: Login,
    contact: Contact,
    organization: Organization,
    activity: Activity,
    activity2: Activity2,
    announce: Announce,
    download: Download,
    faq: FAQ,
    eBook: EBook,
    exhibition: Exhibition,
    projectCalendar: ProjectCalendar,
    phaseSecondProjectMap: PhaseSecondProjectMap,
    phaseFirstProjectMap: PhaseFirstProjectMap,
    blog: Blog,
    report: Report,
    site: Site
  }
});
