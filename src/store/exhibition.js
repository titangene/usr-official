import { debounce } from 'lodash-es';

import API from '@/utils/BackendAPI';
import { ExhibitionTopPostsAPI } from '@/api';
import { formatDate, limitTextCountInHTML } from '@/utils/format';
import { getYouTubeEmbedURL } from '@/utils/helper';

export default {
  namespaced: true,
  state: {
    year: null,
    introduction: null,
    exhibitions: [],
    exhibition: null,
    total: 0
  },
  getters: {
    year(state) {
      return state.year;
    },
    introduction(state) {
      return state.introduction;
    },
    exhibitions(state) {
      return state.exhibitions;
    },
    exhibition(state) {
      return state.exhibition;
    },
    total(state) {
      return state.total;
    }
  },
  mutations: {
    year(state, payload) {
      state.year = payload;
    },
    introduction(state, introduction) {
      state.introduction = introduction;
    },
    exhibitions(state, exhibitions) {
      state.exhibitions = exhibitions.map(restructureExhibitionItem);
    },
    exhibition(state, exhibition) {
      state.exhibition = { ...exhibition };
    },
    total(state, payload) {
      state.total = payload;
    }
  },
  actions: {
    fetchExhibitionsDebounce: debounce(({ dispatch }, pageOptions) => {
      dispatch('fetchExhibitions', pageOptions);
    }, 400),
    async fetchExhibitionById({ commit }, exhibitionId) {
      const { data: exhibition } = await API.POST('emp/read.php', {
        function: 'Permanent_exhibition',
        id: exhibitionId
      });

      if (exhibition) {
        commit('exhibition', restructureExhibitionItem(exhibition));
      }
    },
    async fetchExhibitionTopPost({ commit, getters }) {
      const topPost = getters.year
        ? await ExhibitionTopPostsAPI.getPostByYear(getters.year)
        : await ExhibitionTopPostsAPI.first();

      const introduction = {
        title: topPost.title,
        image: topPost.image,
        primaryYouTubeEmbedURL: getYouTubeEmbedURL(topPost.youtube1),
        secondaryYouTubeEmbedURL: getYouTubeEmbedURL(topPost.youtube2),
        content: topPost.content,
        manual: topPost.manual,
        result: topPost.result
      };
      commit('introduction', introduction);
    },
    async fetchExhibitions(
      { commit, getters },
      { currentPage, pageSize } = {}
    ) {
      const { total, data } = await API.POST('emp/read.php', {
        function: 'Permanent_exhibition',
        years: getters.year ?? '',
        page: currentPage ?? 1,
        page_num: pageSize ?? 10
      });
      commit('exhibitions', data);
      commit('total', total);
    }
  }
};

function restructureExhibitionItem(exhibition) {
  const {
    description,
    opendate,
    video_url: videoURL,
    Small_picture: cover,
    ...otherProps
  } = exhibition;

  return {
    videoURL,
    youTubeEmbedURL: getYouTubeEmbedURL(videoURL),
    cover,
    opendate: formatDate(opendate),
    description: limitTextCountInHTML(description, 20),
    ...otherProps
  };
}
