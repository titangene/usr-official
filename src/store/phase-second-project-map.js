// 所有行政區的經緯度
import latlngOfAdministrativeDistricts from '@/assets/data/administrativeDistrict.json';

import i18n from '@/plugins/vue-i18n';
import API from '@/utils/BackendAPI';

export default {
  namespaced: true,
  state: {
    filterFields: {
      category: [], // 計畫類別
      issue: [], // 議題分類
      SDGs: [], // SDGs
      keyword: '', // 關鍵字

      city: null, // 二級行政區 (縣市)
      district: null, // 三級行政區 (鄉鎮市區)
      region: null // 地區 (二級行政區、三級行政區 或 二級 + 三級行政區)
    },

    categoryOptions: [],
    issueOptions: [],
    SDGsOptions: [],

    regionalOptions: [],
    cityOptions: [],
    districtOptions: [],
    regionalCount: [],

    projects: [],
    total: 0,

    activeProjectId: null,
    activeProject: null
  },
  getters: {
    filterFields(state) {
      return state.filterFields;
    },

    categoryOptions(state) {
      return state.categoryOptions;
    },
    issueOptions(state) {
      return state.issueOptions;
    },
    SDGsOptions(state) {
      return state.SDGsOptions;
    },

    regionalOptions(state) {
      return state.regionalOptions;
    },
    cityOptions(state) {
      return state.cityOptions;
    },
    districtOptions(state, getters) {
      return state.regionalCount
        .filter(region => region.city.value === getters.filterFields.city)
        .map(region => region.district);
    },
    regionalCount(state, getters) {
      return state.regionalCount.map(item => {
        const region = `${item.city.value}${item.district.value}`;

        return {
          count: item.count,
          city: item.city,
          district: item.district,
          region,
          displayRegion:
            i18n.locale === 'zh'
              ? `${item.city.text}${item.district.text}`
              : `${item.city.text}, ${item.district.text}`,
          latLng: getters.getLatlngOfAdministrativeDistricts(region)
        };
      });
    },
    getLatlngOfAdministrativeDistricts() {
      return region =>
        latlngOfAdministrativeDistricts.find(item => item.name === region)
          ?.latlng ?? null;
    },

    projects(state) {
      return state.projects;
    },
    total(state) {
      return state.total;
    },

    activeProjectId(state) {
      return state.activeProjectId;
    },
    activeProject(state) {
      return state.activeProject;
    }
  },
  mutations: {
    filterFields(state, { field, payload }) {
      state.filterFields[field] = payload;
    },

    categoryOptions(state, payload) {
      state.categoryOptions = payload;
    },
    issueOptions(state, payload) {
      state.issueOptions = payload;
    },
    SDGsOptions(state, payload) {
      state.SDGsOptions = payload;
    },

    regionalOptions(state, regionalCount) {
      let cityOptions = {};
      let regionalOptions = {};

      for (const regional of regionalCount) {
        const city = regional.city.value;
        if (!(city in cityOptions)) {
          cityOptions[city] = regional.city;
        }
        regionalOptions[city] = [...(regionalOptions[city] || []), regional];
      }

      const options = Object.entries(regionalOptions).map(
        ([city, regionalOption]) => {
          return {
            city: cityOptions[city],
            districts: regionalOption.map(regional => {
              return { count: regional.count, district: regional.district };
            })
          };
        }
      );

      state.regionalOptions = [
        {
          city: {
            text: i18n.t('PhaseSecondProjectMap.region.overseas'),
            value: '國外場域'
          },
          districts: []
        },
        ...options
      ];
      /* return [
        {
          city: {
            text: 'Taipei City',
            value: '臺北市'
          },
          districts: [
            { name: { text: 'Beitou District', value: '北投區' }, count: 4 }
          ]
        }
      ]; */
    },
    cityOptions(state, regionalCount) {
      let cityOptions = {};

      for (const regional of regionalCount) {
        const city = regional.city.value;
        if (!(city in cityOptions)) {
          cityOptions[city] = regional.city;
        }
      }
      state.cityOptions = [
        {
          text: i18n.t('PhaseSecondProjectMap.region.overseas'),
          value: '國外場域'
        },
        ...Object.values(cityOptions)
      ];
    },
    regionalCount(state, payload) {
      state.regionalCount = payload;
    },

    projects(state, project) {
      state.projects = project.map(restructureProject);
    },
    total(state, payload) {
      state.total = payload;
    },

    activeProjectId(state, payload) {
      state.activeProjectId = payload;
    },
    activeProject(state, payload) {
      state.activeProject = payload ? restructureProject(payload) : null;
    }
  },
  actions: {
    async fetchOptions({ commit }) {
      const options = await API.POST('/emp/read.php', {
        function: 'get_map_2_filter_data',
        language: i18n.locale
      });

      commit('categoryOptions', options.category);
      commit('issueOptions', options.issue);
      commit('SDGsOptions', options.SDGs);
    },
    async fetchProjectRegionalCount({ commit, getters }) {
      const { data: regionalCount } = await API.POST('/emp/read.php', {
        function: 'get_map_2_total_number',
        language: i18n.locale,
        type: getters.filterFields.category,
        class_name: getters.filterFields.issue,
        sdgs: getters.filterFields.SDGs,
        project_or_school: getters.filterFields.keyword,
        project_country: ''
      });

      commit('regionalCount', regionalCount);
      commit('regionalOptions', regionalCount);
      commit('cityOptions', regionalCount);
    },
    async fetchProjectById({ commit }, projectId) {
      const { data: project } = await API.POST('emp/read.php', {
        function: 'project_map_for_2',
        language: i18n.locale,
        id: projectId
      });

      commit('activeProject', project);
    },
    async fetchProjects({ commit, getters }, { currentPage, pageSize } = {}) {
      const region = getters.filterFields.region;
      const regionFields =
        getters.filterFields.region === '國外場域'
          ? { country: '國外場域', project_fields: '' }
          : { country: '國內場域', project_fields: region };

      const { total, data: projects } = await API.POST('/emp/read.php', {
        function: 'project_map_for_2',
        language: i18n.locale,
        type: getters.filterFields.category,
        class_name: getters.filterFields.issue,
        sdgs: getters.filterFields.SDGs,
        project_or_school: getters.filterFields.keyword,

        ...regionFields,

        page: currentPage ?? 1,
        page_num: pageSize ?? 10
      });

      commit('projects', projects);
      commit('total', total);
    },
    async fetchLatLngByRegion({ getters }, region) {
      const searchRegion =
        region ?? getters.filterFields.district ?? getters.filterFields.region;

      const regions = await API.OpenStreetMap.fetchLatLngByRegion(searchRegion);
      const regionalCount = getters.regionalCount.find(item =>
        `${item.city.value}${item.district.value}`.includes(searchRegion)
      );

      if (regions.length === 0) {
        return { type: 'NoSearchResults', value: regionalCount };
      } else {
        const value =
          regions.find(region => {
            return (
              region.category === 'boundary' &&
              region.display_name.includes(getters.filterFields.city)
            );
          }) ?? regions[0];
        return {
          type: 'GetSearchResults',
          value: { ...value, latLng: regionalCount.latLng }
        };
      }
    },
    async fetchCityByLatLng(context, { lat, lng }) {
      // 最差的情況只能知道在台灣，但不知在哪個縣市
      // - 知道縣市：回傳該縣市名稱
      // - 只知道在台灣 (不知道縣市)：回傳 null
      const region = await API.OpenStreetMap.fetchRegionByLatLng({ lat, lng });
      // 縣和市都統一儲存在 `city` 後，再回傳
      const city = region.address.city ?? region.address.county ?? null;
      return city;
    }
  }
};

function restructureProject(project) {
  if (project.contact) {
    const {
      type: category,
      class_name: issue,
      contact: { name, unit, project_job_title: jobTitle, phone, email },
      ...otherProps
    } = project;

    return {
      category,
      issue,
      contact: { name, unit, jobTitle, phone, email },
      ...otherProps
    };
  } else {
    const { type: category, class_name: issue, ...otherProps } = project;

    return {
      category,
      issue,
      ...otherProps
    };
  }
}
