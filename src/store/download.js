import { debounce } from 'lodash-es';

import API from '@/utils/BackendAPI';

export default {
  namespaced: true,
  state: {
    category: null,
    keyword: '',
    files: [],
    total: 0
  },
  getters: {
    category(state) {
      return state.category;
    },
    keyword(state) {
      return state.keyword;
    },
    files(state) {
      return state.files;
    },
    total(state) {
      return state.total;
    }
  },
  mutations: {
    category(state, payload) {
      state.category = payload;
    },
    keyword(state, payload) {
      state.keyword = payload;
    },
    files(state, files) {
      state.files = files.map(restructureDownloadFiles);
    },
    total(state, payload) {
      state.total = payload;
    }
  },
  actions: {
    fetchDownloadFilesDebounce: debounce(({ dispatch }, pageOptions) => {
      dispatch('fetchDownloadFiles', pageOptions);
    }, 400),
    async fetchDownloadFiles({ commit, getters }, options = {}) {
      const { total, data: downloadFiles } = await API.POST('emp/read.php', {
        function: 'download',
        class: getters.category ?? '',
        key_word: getters.keyword,
        page: options.currentPage ?? 1,
        page_num: options.pageSize ?? 10
      });

      commit('total', total);
      commit('files', downloadFiles);
    }
  }
};

function restructureDownloadFiles(announce) {
  const {
    // eslint-disable-next-line no-unused-vars
    create_date,
    File_name: name,
    File_url: url,
    class_name: category,
    ...otherProps
  } = announce;

  return {
    name,
    url,
    category,
    ...otherProps
  };
}
