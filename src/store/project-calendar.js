import { flow } from 'lodash-es';
import { format } from 'date-fns/esm//fp';

import API from '@/utils/BackendAPI';
import {
  _formatDate,
  parseDate,
  parseDateTimeAndFormatDate
} from '@/utils/format';
import { groupByToArray } from '@/utils/helper';
import { sortDate } from '@/utils/dateHelper';

export default {
  namespaced: true,
  state: {
    filterTimeType: '單日搜尋',
    filterFields: {
      singleDay: _formatDate(new Date()),
      rangeDate: [],
      district: null, // 區域
      category: null, // 計畫類型
      schedule: null, // 行程類型
      keyword: '',
      schools: []
    },
    projects: [],
    projectsID: [],
    total: null
  },
  getters: {
    filterTimeType(state) {
      return state.filterTimeType;
    },
    filterFields(state) {
      return state.filterFields;
    },
    projects(state) {
      return state.projects;
    },
    projectsID(state) {
      return state.projectsID;
    },
    total(state) {
      return state.total;
    }
  },
  mutations: {
    filterTimeType(state, payload) {
      state.filterTimeType = payload;
    },
    filterFields(state, { field, payload }) {
      state.filterFields[field] = payload;
    },
    projectsID(state, projects) {
      state.projectsID = projects.map(project => project.id);
    },
    projects(state, projects) {
      const group = groupByToArray(
        projects.map(restructureProject),
        { keyName: 'date', valueName: 'projectsBySchoolGroup' },
        project => project.date
      ).map(group => {
        return {
          date: flow(parseDate, format('yyyy年MM月dd日'))(group.date),
          projectCount: group.projectsBySchoolGroup.length,
          projectsBySchoolGroup: groupByToArray(
            group.projectsBySchoolGroup,
            { keyName: 'school', valueName: 'projects' },
            'school'
          )
        };
      });
      state.projects = sortDate(group);
    },
    total(state, payload) {
      state.total = payload;
    },
    resetTotal(state) {
      state.total = null;
    }
  },
  actions: {
    async fetchProjects({ commit, getters }) {
      commit('resetTotal');

      const [fromDate, toDate] = getters.filterFields.rangeDate;
      const singleDay =
        getters.filterTimeType === '單日搜尋'
          ? getters.filterFields.singleDay
          : '';

      const { data: projects } = await API.POST('/emp/read.php', {
        function: 'calendar_2',
        district_name: getters.filterFields.district ?? '',
        type: getters.filterFields.category ?? '',
        class_name: getters.filterFields.schedule ?? '',
        title: getters.filterFields.keyword,
        date_from: getters.filterTimeType === '範圍搜尋' ? fromDate : '',
        date_to: getters.filterTimeType === '範圍搜尋' ? toDate : '',
        schedule_at: singleDay,
        school: getters.filterFields.schools
      });

      commit('projects', projects);
      commit('projectsID', projects);
      commit('total', projects.length);
    }
  }
};

function restructureProject(project) {
  const {
    project_title: title,
    schedule_at: date,
    type: category,
    class: schedule,
    ...otherProps
  } = project;

  return {
    title,
    date: parseDateTimeAndFormatDate(date),
    category,
    schedule,
    ...otherProps
  };
}
