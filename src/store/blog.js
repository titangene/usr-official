import { debounce } from 'lodash-es';

import API from '@/utils/BackendAPI';
import { formatDate, limitTextCountInHTML } from '@/utils/format';

export default {
  namespaced: true,
  state: {
    year: null,
    keyword: '',
    topBlog: {
      image: null,
      date: null,
      title: '',
      description: ''
    },
    blogs: [],
    otherBlogs: [],
    blog: null,
    total: 0
  },
  getters: {
    year(state) {
      return state.year;
    },
    keyword(state) {
      return state.keyword;
    },
    topBlog(state) {
      return state.topBlog;
    },
    blogs(state) {
      return state.blogs;
    },
    otherBlogs(state, getters) {
      return state.otherBlogs
        .filter(blog => blog.id !== getters.blog.id)
        .slice(0, 4);
    },
    blog(state) {
      return state.blog;
    },
    total(state) {
      return state.total;
    }
  },
  mutations: {
    year(state, payload) {
      state.year = payload;
    },
    keyword(state, payload) {
      state.keyword = payload;
    },
    topBlog(state, blog) {
      state.topBlog = restructureBlogTop(blog);
    },
    blogs(state, blogs) {
      state.blogs = blogs.map(restructureBlog);
    },
    otherBlogs(state, otherBlogs) {
      state.otherBlogs = otherBlogs.map(restructureBlog);
    },
    blog(state, blog) {
      state.blog = restructureBlog(blog);
    },
    total(state, payload) {
      state.total = payload;
    }
  },
  actions: {
    fetchBlogsDebounce: debounce(({ dispatch }, pageOptions) => {
      dispatch('fetchBlogs', pageOptions);
    }, 400),
    async fetchBlogById({ commit }, blogId) {
      const { data: blog } = await API.POST('emp/read.php', {
        function: 'usr_blog',
        id: blogId
      });

      if (blog) {
        commit('blog', blog);
      }
    },
    async fetchBlogs({ commit, getters }, { currentPage, pageSize } = {}) {
      const { total, data: blogs, top: topBlog } = await API.POST(
        'emp/read.php',
        {
          function: 'usr_blog',
          years: getters.year ?? '',
          title: getters.keyword,
          page: currentPage ?? 1,
          page_num: pageSize ?? 10
        }
      );

      commit('total', total);
      commit('blogs', blogs);
      commit('topBlog', topBlog);
    },
    async fetchOtherBlogs({ commit }) {
      const { data: otherBlogs } = await API.POST('emp/read.php', {
        function: 'usr_blog',
        page: 1,
        page_num: 10
      });
      commit('otherBlogs', otherBlogs);
    }
  }
};

function restructureBlogTop(blog) {
  const { description, time, Small_picture: image, ...otherProps } = blog;

  return {
    image,
    date: formatDate(time),
    description: limitTextCountInHTML(description, 170),
    ...otherProps
  };
}

function restructureBlog(blog) {
  const { time, Small_picture: image, ...otherProps } = blog;

  return {
    image,
    date: formatDate(time),
    ...otherProps
  };
}
