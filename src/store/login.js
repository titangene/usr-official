import API from '@/utils/BackendAPI';

export default {
  namespaced: true,
  state: {
    account: '',
    password: '',
    invalidFeedback: '',
    result: {}
  },
  getters: {
    loginParam(state) {
      return {
        account: state.account,
        password: state.password
      };
    },
    account(state) {
      return state.account;
    },
    password(state) {
      return state.password;
    },
    invalidFeedback(state) {
      return state.invalidFeedback;
    },
    result(state) {
      return state.result;
    }
  },
  mutations: {
    account(state, account) {
      state.account = account;
    },
    password(state, password) {
      state.password = password;
    },
    invalidFeedback(state, invalidFeedback) {
      if (invalidFeedback.includes('登入成功')) {
        state.invalidFeedback = '';
      } else if (invalidFeedback.includes('密碼錯誤')) {
        state.invalidFeedback = '密碼錯誤';
      } else if (invalidFeedback.includes('找不到該帳號')) {
        state.invalidFeedback = '找不到該帳號';
      } else {
        state.invalidFeedback = invalidFeedback;
      }
    },
    result(state, result) {
      state.result = result;
    }
  },
  actions: {
    resetForm({ commit }) {
      commit('account', '');
      commit('password', '');
    },
    async login({ getters, commit }) {
      if (getters.account.length === 0 || getters.password.length === 0) {
        commit('invalidFeedback', '請輸入帳號和密碼');
        commit('result', { isLoginSuccess: false });
        return;
      }

      const loginResult = await API.login({
        account: getters.account,
        password: getters.password
      });

      commit('invalidFeedback', loginResult.msg);

      if (loginResult.code != 200) {
        commit('result', { isLoginSuccess: false });
      } else {
        commit('result', {
          isLoginSuccess: true,
          loginDashboardURL: loginResult.url
        });
      }
    }
  }
};
