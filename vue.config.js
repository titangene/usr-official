function productionPublicPath() {
  switch (process.env.DEPOLY_TYPE) {
    case 'GitLabPage':
      return '/usr-official/';
    default:
      return '/';
  }
}

module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production' ? productionPublicPath() : '/',
  pages: {
    index: {
      entry: 'src/main.js',
      title: '教育部大學社會責任推動中心'
    }
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: `
          @import "@/assets/scss/shared.scss";
        `
      },
      less: {
        lessOptions: {
          sourceMap: true,
          javascriptEnabled: true
        }
      }
    }
  },
  chainWebpack: config => {
    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule
      .use('babel-loader')
      .loader('babel-loader')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader');

    const i18nRule = config.module.rule('i18n');
    i18nRule
      .resourceQuery(/blockType=i18n/)
      .type('javascript/auto')
      .use('i18n')
      .loader('@kazupon/vue-i18n-loader')
      .end()
      .use('yaml')
      .loader('yaml-loader')
      .end();

    const yamlRule = config.module.rule('yaml');
    yamlRule
      .test(/\.ya?ml$/)
      .type('json')
      .use('yaml')
      .loader('yaml-loader');
  }
};
